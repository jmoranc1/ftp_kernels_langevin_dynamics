# 02_First_time_passage

This code is used for Langevin Dynamics first-time-passage collision kernels calculations

## Installation

Install requirements
    - Install Cmake: [https://cmake.org/install/](https://cmake.org/install/)
    - Install boost library: sudo apt-get install libboost-all-dev

Download the current version of the code

    git clone git@gitlab.com:jmoranc1/ftp_langevin_dynamics.git
    
Compile the code

    mkdir build
    cd build
    cmake ..
    make all

## Update

Go to the "ftp_langevin_dynamics" folder and type in the terminal:

    git pull
    
Then, recompile the new version (you may first delete all the files within the "build" folder).

## Python

Using Python for post-processing is not mandatory but highly recomended. To use it, you should use the virtual environment automatically created during compilation. Activate the virtual environment by typing in a terminal,

    source venv/bin/activate

Once activated you can compile python codes or use jupyter notebook by typing in the terminal,

    jupyter notebook


## Documentation

Open the html file found in the following directory: **docs/_build/html/index.html**. You should see the following page in your internet explorer:

![Python documentation](docs/Support/01_docs.png "Python documentation")

By clicking on: "Module index" and navegating through the different modules you will find the description of each Python function used in this directory.

## License

FTP_kernels_Langevin_Dynamics is an open-source package, this mean you can use or modify it under the terms and conditions of the GPL-v3 licence. You should have received a copy along with this package, if not please refer to [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).

## Please cite us!
If you use some or all the codes provided in this repository you are kindly asked to cite our work:

Morán, J., & Kholghy, M. R. (2023). Theoretical derivation of particle collision kernels and its enhancement at high concentration from a first-time-passage approach in the diffusive regime. Aerosol Science and Technology, In press. [https://doi.org/10.1080/02786826.2023.2215277](https://doi.org/10.1080/02786826.2023.2215277)

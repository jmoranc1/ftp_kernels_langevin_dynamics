#!/usr/bin/env python3
# coding=utf-8
"""Compile and install Py_FTP_Langevin"""
import sys

from setuptools import setup as setup
from setuptools import Extension as Extension

setup()

# noinspection PyPep8
import numpy as np

Py_FTP_Langevin = Extension(name='Py_FTP_Langevin.ftp_langevin.main_functions',
                     sources=['Py_FTP_Langevin/ftp_langevin/main_functions.pyx','src/main.cpp', 'src/ftp.cpp',
                     'src/tools.cpp','src/particle_list.cpp', 'src/model.cpp','src/particle.cpp'],
                     include_dirs=[np.get_include(),'include', 'src'],
                     extra_compile_args=["-fopenmp", "-O3"],
                     extra_link_args=["-fopenmp"],
                     language="c++")
# noinspection PyUnusedName
Py_FTP_Langevin.cython_c_in_temp = True

setup(ext_modules=[Py_FTP_Langevin])

/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MODEL_HPP
#define MODEL_HPP

#include "parameters.hpp"

using namespace std;

class Model
{
private:
public:
    double MM;
    double Rho_p;
    size_t N_runs;
    double Box_L_factor_dmax;
    double T_g;
    double P_g;
    double phi_p;
    double mu_g;
    double lambda_g;
    size_t run_taus_min;
    double LD_dt_factor;
    double A;
    double s_LJ;
    double alpha_dp;
    double alpha_dp_max;
    double factor_vdw;
    double vdw_cutoff;
    double D_pp;
    bool with_sticking;
    bool with_rel_motion;
    bool with_rotation;
    bool with_brownian;
    std::string path;
    /// Constructor
    Model();
    Model(std::string new_path);
    void Determine_lambda_mu();
    void Set_properties_default();
    void Set_properties(double new_MM,
                        double new_Rhop,
                        size_t new_N_runs,
                        double new_box_factor,
                        double new_T,
                        double new_P,
                        double new_phi,
                        size_t new_runs_taus,
                        double new_LD_fact,
                        double new_A,
                        double new_S_LJ,
                        double new_alpha_dp,
                        double new_alpha_dp_max,
                        double new_factor_vdw,
                        double new_D_pp,
                        double new_vdW_cutoff,
                        bool new_with_collisions,
                        bool new_with_rel_motion,
                        bool new_with_rotation,
                        bool new_with_brownian,
                        std::string new_path);
    void Show_properties() const;
    /// Destructor
    ~Model() {}
};

#endif // MODEL_HPP

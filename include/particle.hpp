/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include "model.hpp"

using namespace std;

class particle
{
private:
public:
    double Rp, rmax;                ///< radius, maximum radius
    double x, y, z;                 ///< position
    double x_, y_, z_;              ///< position after rotation
    double Kn, Cc;                  ///< Knudsen, Cunningham
    double m, f, Di, alpha, d_time; ///< mass, friction, diffusion coefficient, alpha
    double G, H, I;                 ///< values for Langevin Dynamics
    double v[3], v_ini[3];          ///< velocity vectorm v_ini to reestart condition when deleting particles
    double t_res;                   ///< physical residence time
    double r_seed, r_c;             ///< position of seeding, coag. radius
    double tau;                     ///< momentum relaxation time
    double c_rms;                   ///< rms Maxwellian velocity
    double rg;                      ///< radius of gyration
    double df;                      ///< fractal dimension
    double lambda_p;                ///< aggregate persistant distance
    size_t Np;                      ///< number of monomers
    double R_ran[3], V_ran[3];      ///< Langevin position/velocity fluctuation
    double l0, l1, l2, d_mag;       ///< unitary components, and d_mag maginitude displac.
    double displac_collision;       ///< displacement considering collision
    double relative_distance;       ///< relative displacement between agglomerates
    double f_atr, f_rep, f_mag;     ///< Attractive, repulsive, and total interaction force
    double F_ext[3];                ///< external force vector
    bool langevin_check;            ///< a bool to check Langevin accuracy
    size_t label;
    double f_rot;
    /// Constructor
    particle();
    particle(double radius);
    particle(double radius, const Model model);
    particle(double pos_x,
             double pos_y,
             double pos_z,
             double radius,
             const Model model);
    void particle_restart(double radius, const Model model);
    void aggregate_update();
    void new_VR_ran(const double Y[6]);
    /// Destructor
    ~particle() {}
};

#endif // PARTICLE_HPP

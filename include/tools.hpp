/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef TOOLS_HPP
#define TOOLS_HPP

#include "particle_list.hpp"

using std::time;

void InitRandom();

void create_output_folder();

__attribute((const)) double inverfc(const double pp);
__attribute((const)) double inverf(const double pp);
double Get_Lognormal_rand(const double Dp, const double Dp_sigmaG);
double Get_normal_rand(const double mean, const double sigma);
inline double rand_01() { return double(rand()) / (RAND_MAX); };

int number_lines(std::string dir);
void out_pos(std::vector<particle> p,
             size_t counter,
             size_t Np);
size_t getdir(std::string dir,
              std::vector<std::string> &files);
vector<std::string> getdir_files(std::string dir);
particle_list load_agg_external_getline(const std::string dir,
                                        const Model model);
particle_list load_agg_external_ifstream(const std::string dir,
                                         const Model model);
bool Check_commas(std::string line);
void load_agg_external(const std::string dir,
                       const Model model,
                       particle_list &spheres);
void InitRandom();
void Get_rand_gauss(double (&Y)[6]);
void Rand_point_sphere(vector<double> &point);
void SUB_show_parameters(const particle_list spheres,
                         const Model model);
double Vector_norm(const double x,
                   const double y,
                   const double z);
void Cross_product(const double a[3],
                   const double b[3],
                   double (&c)[3]);
#define POW2(a) ((a) * (a))
#define POW3(a) ((a) * (a) * (a))

#endif // TOOLS_HPP
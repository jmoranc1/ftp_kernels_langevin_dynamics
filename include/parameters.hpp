/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PARAMETERS_HPP
#define PARAMETERS_HPP

#include<iostream>
#include<math.h>
#include<fstream>
#include<stdlib.h>
#include<ctime>
#include<algorithm>
#include <map>
#include<vector>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <dirent.h>
#include <errno.h>
#include <string>

//! *********** CONSTANTS *********** !
const double PI = 4.0*atan(1.0);      //!pi number 3.14
const double k_B = 1.380650524e-23; //!Boltzman constant

const double Ru = 8314.472;		    //!Universal ideal gas constant (J/kmol*K)
const double T0 = 293.15;		    //!Reference temperature for Sutherland equation (K)
const double mu_0 = 18.203e-6;	    //!Reference viscosity of air for Sutherland equation (Pa*s)
const double Su = 110.4;		    //!Sutherland interpolation constant (K)

const double A1_t = 1.21;		        //!Factor for translational Cunningham factor
const double A2_t = 0.40;		        //!Factor for translational Cunningham factor
const double A3_t = 1.56;		        //!Factor for translational Cunningham factor

const double A1_r = 3.9300;		        //!Factor for rotational Cunningham factor
const double A2_r = 2.0580;		        //!Factor for rotational Cunningham factor
const double A3_r = 0.3277;		        //!Factor for rotational Cunningham factor

//! *********** PARAMETERS REBOUND *********** !
const double Z = 0.4e-09;       //minimum separation assumed 0.4 nm (Critical velocity)

/*
! *********** SOME REFERENCES *********** !
! Langevin Dynamics: Ermak, D. L., & Buckholz, H. (1980).  J. Comp. Phys., 35(2), 169-182. (Equations 6-7)
! Time step: Suresh, V., & Gopalakrishnan, R. (2021). J. Aerosol Sci., 155, 105746.
! For viscosity, the Sutherland model and values: AEROSOL MEASUREMENT 3rd edition, Kulkarni et al. (page 19)
! For gas particles mean free path: Atmospheric Chemistry and Physics 2nd edition, Seinfeld & Pandis (page 399)
! For Cunnigham correction factor parameters (for air): Rader, (1990). J of aerosol sci, 21(2), 161-168.
! For rotational/translational friction coefficient of agglomerates: Corson, J., et al. (2018). Aerosol Sci. Tech., 52(2), 209-221.
! For random numbers, Box and Muller method: Computational Granular Dynamics, Poschel and Schwager (2005)
! For checking collisions, Linked cell method: Computational Granular Dynamics, Poschel and Schwager (2005)
*/

#endif // PARAMETERS_HPP

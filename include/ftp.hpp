/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef FTP_HPP
#define FTP_HPP

#include "tools.hpp"

using namespace std;

class FTP
{
    friend class particle_list;
    friend class Model;

private:
public:
    particle_list spheres1, spheres2;
    double L_box;
    double d_time;
    double t_res;
    double d_ij_min;
    double Kn_d_ij;
    double m_ij;
    double f_ij;
    double relative_distance;
    double rc_agg;
    bool collision;
    bool first;
    /// Constructor
    FTP();
    FTP(Model model);
    /// Destructor
    ~FTP() {}
    /// Other functions
    void Restart(Model model);
    void Load_aggreg_external(const std::string dir,
                              Model model);
    void Set_spherelist(particle_list in_spheres1,
                        particle_list in_spheres2);
    double Attractive_interaction(const double d,
                                  const double Rp1,
                                  const double Rp2,
                                  const double A);
    double Repulsive_interaction(const double d,
                                 const double R_p1,
                                 const double R_p2,
                                 const double A,
                                 const double s_LJ);
    bool Interaction_potentials(const double A,
                                const double s_LJ,
                                const double factor_vdw,
                                const double vdw_cutoff);
    double Attractive_point(const double d,
                            const double Rp,
                            const double A);
    void One_rw_step_LD_method2(Model model);
    void One_rw_step_LD(Model model);
    void One_rw_step_LD_rel_motion(Model model);
    bool Multi_rw_step_LD(Model model, const size_t n_steps);
    void Random_initial_location();
    void Collisions(const double factor_vdw);
    void Effective_displacement();
    void Move_to_origin();
    void Periodic_boundary_condition();
    void Aggregate_displacement(const double displacement[3]);
    double Determine_time_step(Model model);
    double Determine_time_step_rel_LD(Model model);
    double Relative_distance();
    void new_VR_random_relative(const Model model,
                                const double exp1_sph1,
                                const double exp1_sph2,
                                const double r1_sph1,
                                const double r1_sph2);
    double Gaussian(double mu, double sigma);
    void One_rw_step_rotation_LD(Model model);
};

#endif // FTP_HPP

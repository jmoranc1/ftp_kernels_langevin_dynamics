/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PARTICLE_LIST_HPP
#define PARTICLE_LIST_HPP

#include "particle.hpp"

class particle_list
{
    friend class particle;

private:
public:
    std::vector<particle> list;
    double x_cm, y_cm, z_cm;
    double mean_Rp;
    double Rg;
    double Rmax;
    size_t Np;
    double Rpp_v;                   ///< Vol-eq mean PP radius
    double x_, y_, z_;              ///< position after rotation
    double tau;                     ///< momentum relaxation time
    double v[3], v_ini[3];          ///< velocity vectorm v_ini to reestart condition when deleting particles
    double Kn, Cc;                  ///< Knudsen, Cunningham
    double lambda_p;
    double Kn_d;
    double m, f, Di, alpha, d_time; ///< mass, friction, diffusion coefficient, alpha
    double R_ran[3], V_ran[3];      ///< Langevin position/velocity fluctuation
    double f_atr, f_rep, f_mag;     ///< Attractive, repulsive, and total interaction force
    double f_point;
    double F_ext[3];          ///< external force vector
    bool langevin_check;      ///< a bool to check Langevin accuracy
    double l0, l1, l2, d_mag; ///< unitary components, and d_mag maginitude displac.
    double displac_collision; ///< displacement considering collision
    double relative_distance; ///< relative displacement between agglomerates
    double B1_sq, B2_sq;      ///< values for Langevin Dynamics
    double exp1, exp2, r1;    ///< values for Langevin Dynamics
    double inertia_moment_mag;
    double inertia_moment[3];
    double f_rot;
    double v_rot[3];                ///< Rotation velocity and orientation
    double n_or[3], n_or_old[3];
    double M_ext[3];                ///< external torque (moment)
    double tau_rot;
    double Rs;                      ///< Smoluchowski radius
    double Di_rot;                  ///< Rotational diffusion coefficient
    bool first;
    /// Constructor
    particle_list();
    particle_list(double radius);
    void Show_properties();
    void Show_coordinates();
    void Copy(particle_list in_spheres, const Model model);
    void Set_particle_list(const std::vector<particle> part,
                           const Model model);
    void Update_particle_list(const Model model);
    void Reset_particle_list();
    void Set_new_PP_radius(const double new_Rp);
    void Get_inertia_moment_tensor();
    void Get_mean_rp();
    void Get_radius_gyration();
    void Get_center_mass();
    void Get_maximum_radius();
    void Physical_properties(const Model model);
    void friction_coeff_CORSON(const Model model);
    void new_VR_ran(const Model model,
                    const double time_step);
    double Friction_sphere(const double R, const Model model);
    void Smoluchowski_radius(const Model model);
    double Bisection(const double R0, const Model model);
    void Get_particles(double x[],
                       double y[],
                       double z[],
                       double r[]);
    double Gaussian(double mu, double sigma);
    std::vector<double> Get_particles_x();
    std::vector<double> Get_particles_y();
    std::vector<double> Get_particles_z();
    std::vector<double> Get_particles_Rp();
    void rotate_agglomerate_axes();
    void rotate_random();
    void Rand_point_sphere(std::array<double, 3> &point);
    double rand_01();
    std::array<double, 3> Angles_rotation();
    void matmul_31(const double A[3][3],
                   const double B[3],
                   double (&mult)[3]);
    void matmul_33(const double A[3][3],
                   const double B[3][3],
                   double (&mult)[3][3]);
    double Get_theta(double n1[2], double n2[2]);
    /// Destructor
    ~particle_list() {}
};

#endif // PARTICLE_LIST_HPP

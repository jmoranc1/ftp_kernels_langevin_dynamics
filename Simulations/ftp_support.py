#!/usr/bin/env python3

import dump
import numpy as np
import pandas as pd
from tqdm import tqdm
import matplotlib.pyplot as plt
import random
from plyer import notification
import os,sys,inspect
import multiprocessing as mp
from datetime import datetime

k_B = 1.38066e-23    #! Boltzmann constant in (J/K)
Na = 6.02214076 * pow(10,23) # 1/mol

print("Number of processors: ", mp.cpu_count())

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

ovito_file_name = datetime.now().strftime("%Y_%m_%d-%I:%M:%S_%p")

def Save_partial_results(Kn_d_j,H,ftp_times_j,ftp_times_std_j,tau_12_j,Kn_g_j,
                              L_j,Rc_j,V_box,t_ij,A_j,k_BT,f_12_j,beta_j,
                        name="Sphere_with_vdw_ballistic_no_rot_v01"):
    results_exp = np.column_stack((Kn_d_j,H,ftp_times_j,ftp_times_std_j,tau_12_j,Kn_g_j,
                              L_j,Rc_j,V_box,t_ij,A_j/k_BT,f_12_j,beta_j))
    file_name = "RES_out/"+name+".dat"
    np.savetxt(file_name, results_exp, delimiter=",")
    return

from scipy.optimize import curve_fit

def exponential_fit(x, a,b):
    res = a * np.exp(-b * x)
    return res

def Plot_histogram_ftp(ftp_times,V_box,k,KnD,
                       title='pdf_ftp_diffusive',
                       k_text="k_d",
                       new_figure=True,
                       legend=True):
    t = np.array(ftp_times)
    n = 2/V_box
    
    mean_ftp = np.mean(ftp_times)

    k_freq = k*n/2
    #bins = np.logspace(np.log10(np.min(ftp_times)),np.log10(np.max(ftp_times)), 50)
    counts, bin_edges = np.histogram(t, bins=50, density=True)

    bin_center = np.zeros_like(counts)
    for i in range(len(counts)):
        bin_center[i] = (bin_edges[i]+bin_edges[i+1])/2
    del bin_edges
    popt, pcov = curve_fit(exponential_fit, bin_center,counts, p0=[0.0012, 0.001])
    
    if(new_figure):
        fig, ax1 = plt.subplots(num=None, figsize=(10, 6), dpi=80, facecolor='w', edgecolor='k')
    plt.rc('xtick', labelsize=17) 
    plt.rc('ytick', labelsize=17) 
    plt.plot(bin_center/mean_ftp,counts,"o b",label="Simulation")
    #plt.plot(bin_center, exponential_fit(bin_center, *popt),"-r",label="Exponential fit")
    plt.plot(bin_center/mean_ftp, exponential_fit(bin_center, k_freq,k_freq),"-r",
         linewidth=3,label="Theoretical f(t)")
    #plt.plot(bin_edges[1:], exponential_fit(bin_edges[1:], 0.0012, 0.001),"-g",label="Exponential fit")
    ax1.tick_params(direction='in', length=6, width=1, colors='k',
               grid_color='k', grid_alpha=0.5)
    ax1.tick_params(axis='x', which='minor', direction='in')#,bottom=False)
    ax1.tick_params(axis='y', which='minor', direction='in')#,bottom=False)
    ax1.set_xlabel('First time passage, $t/$E[t] (-)', fontsize=20)
    ax1.set_ylabel('pdf (-)', fontsize=20)
    y_min = np.min(counts); y_max = np.max(counts)
    x_min = np.min(bin_center/mean_ftp); x_max = np.max(bin_center/mean_ftp)
    ax1.text(0.65*(x_max-x_min)+x_min,
         0.2*(y_max-y_min)+y_min,
         "$f(t)="+k_text+"\exp(-"+k_text+" t)$\n"+"Kn$_D=$"+str(round(KnD,2)),
         color="k",
         fontsize=20)
    #ax1.set_xscale("log")
    if(legend):
        plt.legend(fontsize=20); plt.show();
    plt.savefig('Figures/'+title+'.png');
    return

def Export_current_sim(ftp_model,i,normalize=False):
    x1,y1,z1,r1 = ftp_model.Get_spheres(1)
    x2,y2,z2,r2 = ftp_model.Get_spheres(2)
    ofname = "ovito_out/OVITO_" +ovito_file_name+".dump"
    radius = np.concatenate((r1, r2))
    if (normalize):
        nor_factor = np.mean(radius)
        radius = radius/nor_factor
    box_l = ftp_model.L_box/nor_factor
    box = ((-0.5*box_l,0.5*box_l),\
           (-0.5*box_l,0.5*box_l),\
           (-0.5*box_l,0.5*box_l))
    pos = np.array([np.concatenate((x1/nor_factor, x2/nor_factor)),
                   np.concatenate((y1/nor_factor, y2/nor_factor)),
                   np.concatenate((z1/nor_factor, z2/nor_factor))]).transpose()
    vel = pos *0
    
    N = len(x1)+len(x2)
    dump.writeOutput(ofname, N, i, box, radius=radius, pos=pos, v=vel)
    return

def notify_end():
    notification.notify(
    title = "FTP Simulations",
        message = "Finished.",
        timeout = 10
    )
#********************************************************************************
# Set the properties of the model
#********************************************************************************
#  double new_MM,
#  double new_Rhop,
#  size_t new_N_runs,
#  double new_box_factor,
#  double new_T,
#  double new_P,
#  double new_phi,
#  size_t new_runs_taus,
#  double new_LD_fact,
#  double new_A,
#  double new_S_LJ,
#  double new_alpha_dp,
#  double new_alpha_dp_max,
#  double new_factor_vdw,
#  double new_D_pp,
#  double new_vdw_cutoff,
#  bool with_sticking,
#  bool with_rel_motion,
#  bool with_rotation,
#  string new_path
def Set_model_properties(model,props):
    model.Set_prop(new_MM = props["new_MM"],
                   new_Rhop = props["new_Rhop"],
                   new_N_runs = props["new_N_runs"],
                   new_box_factor = props["new_box_factor"],
                   new_T = props["new_T"],
                   new_P = props["new_P"],
                   new_phi = props["new_phi"],
                   new_runs_taus = props["new_runs_taus"],
                   new_LD_fact = props["new_LD_fact"],
                   new_A = props["new_A"],
                   new_S_LJ = props["new_S_LJ"],
                   new_alpha_dp = props["new_alpha_dp"],
                   new_alpha_dp_max = props["new_alpha_dp_max"],
                   new_factor_vdw = props["new_factor_vdw"],
                   new_D_pp = props["new_D_pp"],
                   new_vdw_cutoff = props["new_vdw_cutoff"],
                   with_sticking = props["with_sticking"],
                   with_rel_motion = props["with_rel_motion"],
                   with_rotation = props["with_rotation"],
                   with_brownian = props["with_brownian"],
                   new_path = props["new_path"])
    return model

#********************************************************************************
# Dimensionless collision kernels
# From: 
#********************************************************************************
def H_function(Kn_d2):
    cc1 = 25.836
    cc2 = 11.211
    cc3 = 3.502
    cc4 = 7.211
    H_gop = (4.0*np.pi*pow(Kn_d2,2)+cc1*pow(Kn_d2,3)+
        np.sqrt(8.0*np.pi)*cc2*pow(Kn_d2,4))/(1.0+cc3*Kn_d2+
        cc4*pow(Kn_d2,2)+cc2*pow(Kn_d2,3))
    return H_gop
def H_diffusive(Kn_d2):
    H_d = 4*np.pi*pow(Kn_d2,2)
    return H_d
def H_ballistic(Kn_d2):
    H_b = np.sqrt(8*np.pi)*Kn_d2
    return H_b

#********************************************************************************
# Return particle chemical parameters
#********************************************************************************
# Types of nanoparticles analyzed:   
# Ref.: LJ parameters for metallic NPs: 
# - Heinz, H., Vaia, R. A., Farmer, B. L., & Naik, R. R. (2008). Accurate simulation of surfaces and interfaces of face-centered cubic metals using 12− 6 and 9− 6 Lennard-Jones potentials. The Journal of Physical Chemistry C, 112(44), 17281-17290.

# Ref.: Hamaker constants metallic NPs
# - Krupp, H., Schnabel, W., & Walter, G. (1972). The Lifshitz-Van der Waals constant: Computation of the Lifshitz-Van der Waals constant on the basis of optical data. Journal of Colloid and Interface Science, 39(2), 421-423.
# - Tolias, P. (2020). Non-retarded room temperature Hamaker constants between elemental metals. Surface Science, 700, 121652.
def Get_params_particles(part_type):
    if (part_type == "Soot"):
        MM=28.9647                 # kg/kmol
        Rho_p = 1800               # kg/m^3
        epsilon_lj = 0.2599*1000   # J/mol
        sigma_lj = 0.3516e-09      # m
        A = 4.508e-20              # J
    elif (part_type == "Ag"):
        MM=107.8682                 # kg/kmol
        Rho_p=10.49 * 1000          # kg/m^3
        epsilon_lj=19079.04         # J/mol
        sigma_lj=2.955*pow(10,-10)  # m
        A=3.682*pow(10,-19)           # J
    elif(part_type == "Cu"):
        MM=63.546                   # kg/kmol
        Rho_p=8.96 * 1000           # kg/m^3
        epsilon_lj=19748.48         # J/mol 
        sigma_lj=2.616*pow(10,-10)  # m
        A=3.382*pow(10,-19)         # J
    elif (part_type == "Au"):
        MM=196.96657                # kg/kmol
        Rho_p=19.32 * 1000          # kg/m^3
        epsilon_lj=22133.36         # J/mol
        sigma_lj=2.951*pow(10,-10)  # m
        A=4.018*pow(10,-19)         # J
    elif (part_type == "Pb"):
        MM=207.2                    # kg/kmol
        Rho_p=11.35 * 1000          # kg/m^3
        epsilon_lj=12259.12         # J/mol
        sigma_lj=3.565*pow(10,-10)  # m
        # NOTE: Not available A, determined from LJ params:
        M=MM/1000
        rho = Rho_p/(M/Na) 
        A = Determine_Hamaker_constant(epsilon_lj,sigma_lj,rho)
        A = A/Na
    elif (part_type == "Pt"):
        MM=195.084                  # kg/kmol
        Rho_p=21.45 * 1000          # kg/m^3
        epsilon_lj=32635.20         # J/mol
        sigma_lj=2.845*pow(10,-10)  # m
        A=4.501*pow(10,-19)         # J
    elif (part_type == "Pd"):
        MM=106.42                   # kg/kmol
        Rho_p=12.02 * 1000          # kg/m^3
        epsilon_lj=25731.6          # J/mol
        sigma_lj=2.819*pow(10,-10)  # m
        A=3.886*pow(10,-19)         # J
    elif (part_type == "Ni"):
        MM=58.6934                  # kg/kmol
        Rho_p=8.902 * 1000          # kg/m^3
        epsilon_lj=23639.6          # J/mol
        sigma_lj=2.552*pow(10,-10)  # m
        A=3.692*pow(10,-19)         # J
    else:
        print("ERROR: Unidentified element!!")
        MM=Rho_p=epsilon_lj=sigma_lj=A=0
    return MM,Rho_p,epsilon_lj,sigma_lj,A

#********************************************************************************
# Hamaker constant based on LJ parameters
#********************************************************************************
def Determine_Hamaker_constant(epsilon_lj,sigma_lj, rho):
    lambd = 4*epsilon_lj*np.power(sigma_lj,6) #(J/mol)*(m^6)
    A = (np.pi**2)*(rho**2)*lambd # ((1/m^3)*(J/mol)*(m^6))=(J/mol)
    return A

#********************************************************************************
# Potential well depth and location
#********************************************************************************
def Determine_pot_well(Dpi,Dpj,A,sigma_lj,k_BT,points=10000):
    Dp = (Dpi+Dpj) * 0.5
    if(A==0):
        e_well = 0
        r_e_well = Dp
        return e_well,r_e_well
    radial_dist = np.logspace(np.log10(1),np.log10(1.3),points)*Dp*1.00001 # - Dp
    # Interaction potentials
    u_atr = np.zeros_like(radial_dist, dtype=np.float)
    u_rep = np.zeros_like(radial_dist, dtype=np.float)
    u_tot = np.zeros_like(radial_dist, dtype=np.float)
    i=0
    s_LJ = sigma_lj/Dp
    for r in radial_dist:
        x = r/Dpi
        y = Dpi/Dpj
        u_atr[i] = Attractive_potential(x,y,A)
        u_rep[i] = Repulsive_potential(x,y,A,s_LJ)
        u_tot[i] = u_atr[i]+u_rep[i]
        i=i+1
    e_well = np.min(u_tot/k_BT)
    r_e_well = radial_dist[u_tot==np.min(u_tot)]
    return e_well,r_e_well

#********************************************************************************
# Repulsive potential
#********************************************************************************
def Repulsive_potential(x,y, A,s_LJ):
    U_rep0 = A*pow(s_LJ,6)/(37800)
    U_rep1 = (2*pow(x,2) + 7*pow(y,2) + 9*x + 29*y + 9*x*y + 7)\
             /((1+2*x+y)*pow(1+x+y,7))
    U_rep2 = (-2*pow(x,2) - 9*x + 20*y + 5*x*y - 7)\
             /((1+2*x+y)*pow(1+x,7))
    U_rep3 = (-2*pow(x,2) - 7*pow(y,2) + 5*x + 20*y - 9*x*y)\
             /((1+2*x+y)*pow(x+y,7))
    U_rep4 = (2*pow(x,2) - 5*x + 15*y - 5*x*y)\
             /((1+2*x+y)*pow(x,7))
    U_rep = U_rep0*(U_rep1 + U_rep2 + U_rep3 + U_rep4)
    return float(U_rep)

#********************************************************************************
# Attractive interaction potential
#********************************************************************************
def Attractive_potential(x,y, A):
    x = 2*x
    U_atr1 = 2*y / (x**2 - (1+y)**2)
    U_atr2 = 2*y / (x**2 - (1-y)**2)
    U_atr3 = np.log((x**2 - (1+y)**2)/(x**2 - (1-y)**2))
    U_vdw = -(A/6)*(U_atr1 + U_atr2 + U_atr3)
    return float(U_vdw)
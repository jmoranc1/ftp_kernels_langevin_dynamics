#    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

##< coding=utf-8
##< cython: language_level=3
##< cython: initializedcheck=False
##< cython: binding=True
##< cython: nonecheck=False
##< cython: boundscheck=False
##< cython: wraparound=False

from . cimport main_functions
from . cimport model_functions
from . cimport particle_functions
from . cimport tools_functions
from . cimport ftp_functions

from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.string cimport string  # import std::string as string

import numpy as np
cimport numpy as np

##########################################################
#  			Auxiliary Functions
##########################################################
def Py2cpp_string(py_unicode_string):
    cdef string cpp_string = py_unicode_string.encode('UTF-8')
    return cpp_string
      
##########################################################
#  			Main
##########################################################
def main_sim():
    return main_functions.main()

##########################################################
#  			Particles
##########################################################    
cdef class Pyparticle_list:
    cdef particle_functions.particle_list *c_part_list
    def __cinit__(self, double radius):
        self.c_part_list = new particle_functions.particle_list(radius)
    def __dealloc__(self):
        if self.c_part_list != NULL:
            del self.c_part_list
    def Show_prop(self):
        return self.c_part_list.Show_properties()
    @property
    def d_time(self):
        return self.c_part_list.d_time
    @d_time.setter
    def d_time(self, d_time):
        self.c_part_list.d_time = d_time
    @property
    def Rmax(self):
        return self.c_part_list.Rmax
    @Rmax.setter
    def Rmax(self, Rmax):
        self.c_part_list.Rmax = Rmax
    @property
    def d_mag(self):
        return self.c_part_list.d_mag
    @d_mag.setter
    def d_mag(self, d_mag):
        self.c_part_list.d_mag = d_mag
    @property
    def displac_collision(self):
        return self.c_part_list.displac_collision
    @displac_collision.setter
    def displac_collision(self, displac_collision):
        self.c_part_list.displac_collision = displac_collision
    @property
    def relative_distance(self):
        return self.c_part_list.relative_distance
    @property
    def Di(self):
        return self.c_part_list.Di
    @property
    def Kn_d(self):
        return self.c_part_list.Kn_d
    @property
    def inertia_moment(self):
        return self.c_part_list.inertia_moment_mag
    @property
    def total_force_12(self):
        f0 = self.c_part_list.F_ext[0]
        f1 = self.c_part_list.F_ext[1]
        f2 = self.c_part_list.F_ext[2]
        return np.array([f0, f1, f2])

cdef class Pyparticle:
    cdef particle_functions.particle *c_part
    def __cinit__(self, double radius):
        self.c_part = new particle_functions.particle(radius)
    def __dealloc__(self):
        if self.c_part != NULL:
            del self.c_part

##########################################################
#  			Model
##########################################################
cdef class Pymodel:
    cdef model_functions.Model *c_model
    def __cinit__(self, new_path):
        self.c_model = new model_functions.Model(Py2cpp_string(new_path))
    def __dealloc__(self):
        if self.c_model != NULL:
            del self.c_model
    def Set_prop_default(self):
        return self.c_model.Set_properties_default()
    def Show_prop(self):
        return self.c_model.Show_properties()
    def Set_prop(self,double new_MM,
                      double new_Rhop,
        		      size_t new_N_runs,
                      double new_box_factor,
        		      double new_T,
                      double new_P,
        		      double new_phi,
                      size_t new_runs_taus,
        		      double new_LD_fact,
                      double new_A,
        		      double new_S_LJ,
                      double new_alpha_dp,
                      double new_alpha_dp_max,
                      double new_factor_vdw,
                      double new_D_pp,
                      double new_vdw_cutoff,
                      bool with_sticking,
                      bool with_rel_motion,
                      bool with_rotation,
                      bool with_brownian,
                      new_path):
        new_path_cpp = Py2cpp_string(new_path)
        return self.c_model.Set_properties(new_MM,
                                        new_Rhop,
                                        new_N_runs,
        		                        new_box_factor,
                                        new_T,
                                        new_P,
                                        new_phi,
        		                        new_runs_taus,
                                        new_LD_fact,
                                        new_A,
        		                        new_S_LJ,
                                        new_alpha_dp,
                                        new_alpha_dp_max,
                                        new_factor_vdw,
                                        new_D_pp,
                                        new_vdw_cutoff,
                                        with_sticking,
                                        with_rel_motion,
                                        with_rotation,
                                        with_brownian,
                                        new_path_cpp)
    # Attribute access
    @property
    def Box_L_factor_dmax(self):
        return self.c_model.Box_L_factor_dmax
    @Box_L_factor_dmax.setter
    def Box_L_factor_dmax(self, Box_L_factor_dmax):
        self.c_model.Box_L_factor_dmax = Box_L_factor_dmax
    @property
    def N_runs(self):
        return self.c_model.N_runs
    @N_runs.setter
    def N_runs(self, N_runs):
        self.c_model.N_runs = N_runs
    @property
    def A(self):
        return self.c_model.A
    @A.setter
    def A(self, A):
        self.c_model.A = A
    @property
    def s_LJ(self):
        return self.c_model.s_LJ
    @s_LJ.setter
    def s_LJ(self, s_LJ):
        self.c_model.s_LJ = s_LJ

##########################################################
#  			FTP class
##########################################################
cdef class PyFTP:
    cdef ftp_functions.FTP *c_ftp
    def __cinit__(self, Pymodel model):
        self.c_ftp = new ftp_functions.FTP(model.c_model[0])
    def __dealloc__(self):
        if self.c_ftp != NULL:
            del self.c_ftp
    def Restart_ftp(self, Pymodel model):
        return self.c_ftp.Restart(model.c_model[0])
    def Get_Gaussian(self, double mu, double sigma):
        return self.c_ftp.Gaussian(mu, sigma)
    def Attr_interaction(self, double d,
                        double R_p1, double R_p2, double A):
        return self.c_ftp.Attractive_interaction(d, R_p1, R_p2, A)
    def Attr_point(self,
                   double d,
                   double R_p,
                   double A):
        return self.c_ftp.Attractive_point(d, R_p, A)
    def Load_aggregates_external(self, dir, Pymodel model):
        dir_cpp = Py2cpp_string(dir)
        return self.c_ftp.Load_aggreg_external(dir_cpp, model.c_model[0])
    def Rep_interaction(self, double d,
                        double R_p1, double R_p2,
                        double A, double s_LJ):
        return self.c_ftp.Repulsive_interaction(d, R_p1, R_p2, A, s_LJ)
    def Inter_potentials(self, double A, double s_LJ, double factor_vdw, double vdw_cutoff):
        return self.c_ftp.Interaction_potentials(A, s_LJ, factor_vdw, vdw_cutoff)
    def OneRW_step_LD(self, Pymodel model):
        return self.c_ftp.One_rw_step_LD(model.c_model[0])
    def run(self, Pymodel model, int n_steps):
        return self.c_ftp.Multi_rw_step_LD(model.c_model[0], n_steps)
    def Collisions_check(self, double factor_vdw):
        return self.c_ftp.Collisions(factor_vdw)
    def Effect_displacement(self):
        return self.c_ftp.Effective_displacement()
    def Move_2_origin(self):
        return self.c_ftp.Move_to_origin()
    def Agg_displacement(self, displacement):
        cdef double[3] c_displacement = displacement
        return self.c_ftp.Aggregate_displacement(c_displacement)
    def Per_boundary_condition(self):
        return self.c_ftp.Periodic_boundary_condition()
    def Random_init_location(self):
        return self.c_ftp.Random_initial_location()
    def show_spheres(self):
        self.c_ftp.spheres1.Show_properties()
        self.c_ftp.spheres2.Show_properties()
        return
    def Get_spheres(self, int id):
        if(id == 1):
            x = self.c_ftp.spheres1.Get_particles_x()
            y = self.c_ftp.spheres1.Get_particles_y()
            z = self.c_ftp.spheres1.Get_particles_z()
            r = self.c_ftp.spheres1.Get_particles_Rp()
        elif(id == 2):
            x = self.c_ftp.spheres2.Get_particles_x()
            y = self.c_ftp.spheres2.Get_particles_y()
            z = self.c_ftp.spheres2.Get_particles_z()
            r = self.c_ftp.spheres2.Get_particles_Rp()
        py_x = np.asarray(x)
        py_y = np.asarray(y)
        py_z = np.asarray(z)
        py_r = np.asarray(r)
        return py_x, py_y, py_z, py_r
    def Show_coordinates(self, int id):
        if(id == 1):
            self.c_ftp.spheres1.Show_coordinates()
        elif(id == 2):
            self.c_ftp.spheres2.Show_coordinates()
        return
    def Get_orientation(self, int id):
        if (id == 1):
            n_or = self.c_ftp.spheres1.n_or
        elif(id == 2):
            n_or = self.c_ftp.spheres2.n_or
        return n_or[0],n_or[1],n_or[2]
    @property
    def L_box(self):
        return self.c_ftp.L_box
    @L_box.setter
    def L_box(self, double l_b):
        self.c_ftp.L_box = l_b
    @property
    def d_time(self):
        return self.c_ftp.d_time
    @property
    def t_res(self):
        return self.c_ftp.t_res
    @property
    def d_ij_min(self):
        return self.c_ftp.d_ij_min
    @property
    def Kn_d_avg(self):
        Kn_d1 = self.c_ftp.spheres1.Kn_d
        Kn_d2 = self.c_ftp.spheres2.Kn_d
        return 0.5*(Kn_d1+Kn_d2)
    @property
    def Kn_g_avg(self):
        Kn1 = self.c_ftp.spheres1.Kn
        Kn2 = self.c_ftp.spheres2.Kn
        return 0.5*(Kn1+Kn2)
    @property
    def Kn_d_ij(self):
        return self.c_ftp.Kn_d_ij
    @property
    def Tau_ij(self):
        t_ij = self.c_ftp.m_ij/self.c_ftp.f_ij
        return t_ij
    @property
    def f_ij(self):
        return self.c_ftp.f_ij
    @property
    def Rc(self):
        if(self.c_ftp.spheres1.Np == 1):
            Rc1 = self.c_ftp.spheres1.Rs
            Rc2 = self.c_ftp.spheres2.Rs
        return (Rc1+Rc2)
    @property
    def Diff_ij(self):
        D1 = self.c_ftp.spheres1.Di
        D2 = self.c_ftp.spheres2.Di
        return (D1+D2)
    @property
    def Diff_rot_ij(self):
        D1_rot = self.c_ftp.spheres1.Di_rot
        D2_rot = self.c_ftp.spheres2.Di_rot
        return (D1_rot+D2_rot)
    @property
    def Inertia_ij(self):
        I_i = np.array([self.c_ftp.spheres1.inertia_moment[0],
                        self.c_ftp.spheres1.inertia_moment[1],
                        self.c_ftp.spheres1.inertia_moment[2]])
        I_j = np.array([self.c_ftp.spheres2.inertia_moment[0],
                        self.c_ftp.spheres2.inertia_moment[1],
                        self.c_ftp.spheres2.inertia_moment[2]])
        return (I_i+I_j)
    @property 
    def Relative_dist(self):
        return self.c_ftp.Relative_distance()
    @property
    def Total_force_ij(self):
        f0 = self.c_ftp.spheres2.F_ext[0]
        f1 = self.c_ftp.spheres2.F_ext[1]
        f2 = self.c_ftp.spheres2.F_ext[2]
        return np.array([f0, f1, f2])
    @property
    def f_2_fp(self):
        return (self.c_ftp.spheres2.f_mag,self.c_ftp.spheres2.f_point)

##########################################################
#  			Tools
########################################################## 
def Init_Random():
    return tools_functions.InitRandom()

def Make_output_folder():
    return tools_functions.create_output_folder()

def get_dir(string dir, vector[string] &files):
    return tools_functions.getdir(dir, files)

def get_dir_files(dir):
    dir_cpp = Py2cpp_string(dir)
    return tools_functions.getdir_files(dir_cpp)

def load_aggregate_external(dir,
                            Pymodel model,
                            Pyparticle_list spheres):
    dir_cpp = Py2cpp_string(dir)
    tools_functions.load_agg_external(dir_cpp, model.c_model[0], spheres.c_part_list[0])
    return spheres

def Show_parameters(Pyparticle_list spheres, Pymodel model):
    return tools_functions.SUB_show_parameters(spheres.c_part_list[0],
                                               model.c_model[0])

def Get_normal_random(const double mean, const double sigma):
    return tools_functions.Get_normal_rand(mean, sigma)

def Random_point_sphere(vector[double] &point):
    return tools_functions.Rand_point_sphere(point)
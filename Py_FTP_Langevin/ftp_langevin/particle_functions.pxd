#    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from libcpp cimport bool
from libcpp.vector cimport vector

##########################################################
#  			Particle
##########################################################
cdef extern from "particle_list.hpp":
    cdef cppclass particle_list:
        particle_list() except +
        particle_list(double) except +
        void Show_properties()
        vector[double] Get_particles_x()
        vector[double] Get_particles_y()
        vector[double] Get_particles_z()
        vector[double] Get_particles_Rp()
        double x_cm, y_cm, z_cm
        double mean_Rp
        double Rg
        double Rmax
        double Rs
        size_t Np
        double Rpp_v                   #///< Vol-eq mean PP radius
        double x_, y_, z_              #///< position after rotation
        double tau                     #///< momentum relaxation time
        double v[3], v_ini[3]          #///< velocity vectorm v_ini to reestart condition when deleting particles
        double Kn, Cc                  #///< Knudsen, Cunningham
        double m, f, Di, alpha, d_time #///< mass, friction, diffusion coefficient, alpha
        double G, H, I                 #///< values for Langevin Dynamics
        double R_ran[3], V_ran[3]      #///< Langevin position/velocity fluctuation
        double f_atr, f_rep, f_mag     #///< Attractive, repulsive, and total interaction force
        double f_point
        double F_ext[3]                #///< external force vector
        double n_or[3]                 #///< orientation
        bool langevin_check            #///< a bool to check Langevin accuracy
        double l0, l1, l2, d_mag       #///< unitary components, and d_mag maginitude displac.
        double displac_collision       #///< displacement considering collision
        double relative_distance       #///< relative displacement between agglomerates
        double Kn_d
        double Di_rot
        double inertia_moment[3]
        double inertia_moment_mag
        void Show_coordinates()

cdef extern from "particle.hpp":
    cdef cppclass particle:
        particle() except +
        particle(double) except +
        double Rp, rmax;                #///< radius, maximum radius
        double x,y,z;                   #///< position
        double x_,y_,z_;                #///< position after rotation
        double Kn, Cc;                  #///< Knudsen, Cunningham
        double m,f,Di,alpha,d_time;     #///< mass, friction, diffusion coefficient, alpha
        double G,H,I;                   #///< values for Langevin Dynamics
        double v[3],v_ini[3];           #///< velocity vectorm v_ini to reestart condition when deleting particles
        double t_res;                   #///< physical residence time
        double r_seed, r_c;             #///< position of seeding, coag. radius
        double tau;                     #///< momentum relaxation time
        double c_rms;                   #///< rms Maxwellian velocity
        double rg;                      #///< radius of gyration
        double df;                      #///< fractal dimension
        double lambda_p;                #///< aggregate persistant distance
        size_t Np;                      #///< number of monomers
        double R_ran[3], V_ran[3];      #///< Langevin position/velocity fluctuation
        double l0,l1,l2,d_mag;          #///< unitary components, and d_mag maginitude displac.
        double displac_collision;       #///< displacement considering collision
        double relative_distance;       #///< relative displacement between agglomerates
        double f_atr,f_rep,f_mag;       #///< Attractive, repulsive, and total interaction force
        double F_ext[3];                #///< external force vector
        bool langevin_check;            #///< a bool to check Langevin accuracy
        void particle_restart(double)

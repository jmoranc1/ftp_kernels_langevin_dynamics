#    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
#    Copyright (C) 2021  José Morán
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.string cimport string  # import std::string as string

from . cimport particle_functions
from . cimport model_functions

##########################################################
#  			FTP
##########################################################    
cdef extern from "ftp.hpp":
    cdef cppclass FTP:
        FTP() except +
        FTP(model_functions.Model model) except +
        particle_functions.particle_list spheres1
        particle_functions.particle_list spheres2
        double L_box
        double d_time
        double t_res
        double d_ij_min
        double Kn_d_ij
        double m_ij
        double f_ij
        void Restart(model_functions.Model model)
        void Load_aggreg_external(const string dir,
                                  model_functions.Model model)
        double Attractive_interaction(const double d,
                                      const double Rp1,
                                      const double Rp2,
                                      const double A)
        double Repulsive_interaction(const double d,
                                     const double R_p1,
                                     const double R_p2,
                                     const double A,
                                     const double s_LJ)
        double Attractive_point(const double d,
                                const double Rp,
                                const double A)
        void Interaction_potentials(const double A,
                                    const double s_LJ,
                                    const double factor_vdw,
                                    const double vdw_cutoff)
        void One_rw_step_LD(model_functions.Model model)
        bool Multi_rw_step_LD(model_functions.Model model, const size_t n_steps)
        void Random_initial_location()
        void Collisions(const double factor_vdw)
        void Effective_displacement()
        void Move_to_origin()
        void Periodic_boundary_condition()
        void Aggregate_displacement(const double displacement[3])
        double Gaussian(double mu, double sigma)
        double Relative_distance()

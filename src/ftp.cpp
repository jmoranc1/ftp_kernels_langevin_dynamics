/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ftp.hpp"

using namespace std;

FTP::FTP() {}

FTP::FTP(Model model)
{
    L_box = model.Box_L_factor_dmax * (2. * spheres2.Rmax);
    t_res = 0.0;
    Kn_d_ij = 0.0;
    m_ij = 0.0;
    f_ij = 0.0;
    collision = false;
    first= true;
    rc_agg = 0.0;
}

void FTP::Restart(Model model)
{
    //std::cout << "Resarting FTP with properties: " << std::endl;
    //model.Show_properties();
    spheres1.Reset_particle_list();
    spheres2.Reset_particle_list();

    spheres1.Update_particle_list(model);
    spheres2.Update_particle_list(model);
    spheres1.rotate_random();
    spheres2.rotate_random();
    L_box = model.Box_L_factor_dmax * (2. * spheres2.Rmax);
    d_time = spheres2.d_time;
    t_res = 0.0;
    d_ij_min = 0.0;
    collision = false;
    first= true;
    rc_agg = spheres1.Rmax + spheres2.Rmax;
    f_ij = (spheres1.f * spheres2.f)/(spheres1.f + spheres2.f);
    m_ij = (spheres1.m * spheres2.m)/(spheres1.m + spheres2.m);

    Kn_d_ij = std::sqrt(k_B * model.T_g * m_ij)/(f_ij * rc_agg);
}

/********************************************************************************
 * Make both sphere lists a copy of 2 existing ones
 ********************************************************************************/
void FTP::Set_spherelist(particle_list in_spheres1,
                         particle_list in_spheres2)
{
    spheres1 = in_spheres1;
    spheres2 = in_spheres2;
    rc_agg = spheres1.Rmax + spheres2.Rmax;
}

/********************************************************************************
 * Load a .dat file containing the coordinates of spheres of an aggregate
 ********************************************************************************/
void FTP::Load_aggreg_external(const std::string dir,
                               Model model)
{
    spheres1.Copy(load_agg_external_ifstream(dir, model), model);
    spheres2.Copy(load_agg_external_ifstream(dir, model), model);

    spheres1.Update_particle_list(model);
    spheres2.Update_particle_list(model);
    L_box = model.Box_L_factor_dmax * (2. * spheres2.Rmax);
    d_time = spheres2.d_time;
    t_res = 0.0;
    d_ij_min = 0.0;
    rc_agg = spheres1.Rmax + spheres2.Rmax;
    f_ij = (spheres1.f * spheres2.f)/(spheres1.f + spheres2.f);
    m_ij = (spheres1.m * spheres2.m)/(spheres1.m + spheres2.m);

    Kn_d_ij = std::sqrt(k_B * model.T_g * m_ij)/(f_ij * rc_agg);
    collision = false;
}

/********************************************************************************
 * Repulsive interaction potential forces
 ********************************************************************************/
double FTP::Repulsive_interaction(const double d,
                                  const double R_p1,
                                  const double R_p2,
                                  const double A,
                                  const double s_LJ)
{
    double x = d / R_p1;
    double y = R_p2 / R_p1;
    double f_r0 = -3 * A / (18900 * R_p1) * std::pow(s_LJ / R_p1, 6);
    double f_r1 = -(std::pow(x, 3) + 8 * std::pow(x, 2) * (y + 1) +
                    8 * x * (std::pow(y, 2) + 7 * y + 1) + std::pow(y, 3) +
                    8 * std::pow(y, 2) + 8 * y + 1) /
                  (std::pow(x, 2) * pow(x + y + 1, 8));
    double f_r2 = (std::pow(x, 3) - 8 * std::pow(x, 2) * (y - 1) +
                   8 * x * (std::pow(y, 2) - 7 * y + 1) - std::pow(y, 3) +
                   8 * std::pow(y, 2) - 8 * y + 1) /
                  (std::pow(x, 2) * pow(x - y + 1, 8));
    double f_r3 = (std::pow(x, 3) + 8 * std::pow(x, 2) * (y - 1) +
                   8 * x * (std::pow(y, 2) - 7 * y + 1) + std::pow(y, 3) -
                   8 * std::pow(y, 2) + 8 * y - 1) /
                  (std::pow(x, 2) * pow(x + y - 1, 8));
    double f_r4 = -(std::pow(x, 3) - 8 * std::pow(x, 2) * (y + 1) +
                    8 * x * (std::pow(y, 2) + 7 * y + 1) - std::pow(y, 3) -
                    8 * std::pow(y, 2) - 8 * y - 1) /
                  (std::pow(x, 2) * pow(x - y - 1, 8));
    return std::abs(f_r0 * (f_r1 + f_r2 + f_r3 + f_r4));
}

/********************************************************************************
 * Calculate attractive interaction between 2 spherical particles
 ********************************************************************************/
double FTP::Attractive_interaction(const double d,
                                   const double Rp1,
                                   const double Rp2,
                                   const double A)
{
    double x = d / Rp1;
    double y = Rp2 / Rp1;
    double f_atr0 = 32. * A / (3. * Rp1);
    double f_atr1 = x * std::pow(y, 3) / std::pow(std::pow(x, 4) -
                    2. * std::pow(x, 2) * (y * y + 1.) + std::pow(y * y - 1., 2), 2);
    return std::abs(f_atr0 * f_atr1);
}

/********************************************************************************
 * Calculate attractive interaction assuming particles as point masses
 ********************************************************************************/
double FTP::Attractive_point(const double d,
                             const double Rp,
                             const double A)
{
    double temp0 = 32. * A / (3. * Rp);
    double temp1 = std::pow(Rp / d, 7);
    return temp0 * temp1;
}

/********************************************************************************
 * Calculate interaction potential forces
 ********************************************************************************/
bool FTP::Interaction_potentials(const double A,
                                 const double s_LJ,
                                 const double factor_vdw,
                                 const double vdw_cutoff)
{
    bool detected_collision(false);
    double f_0(0), f_1(0), f_2(0);
    double unit_21[3];
    double dist(0), f_tot_21(0), f_atr(0), f_rep(0);
    d_ij_min = 10000000.;

    double M_21[3], d_j_cm2[3], f_21[3];
    double dist_sph2_cm1;
    double m_0(0), m_1(0), m_2(0);

    // This is calculated for vdW comprobation only
    spheres2.f_point = Attractive_point(relative_distance, spheres1.list[0].Rp, A);
    
    // Save CPU time neglecting vdW at long distances
    if (relative_distance > rc_agg * vdw_cutoff)
    {
        unit_21[0] = spheres1.x_cm - spheres2.x_cm;
        unit_21[1] = spheres1.y_cm - spheres2.y_cm;
        unit_21[2] = spheres1.z_cm - spheres2.z_cm;

        // Unit vector pointing from spheres2 to spheres1
        unit_21[0] /= relative_distance;
        unit_21[1] /= relative_distance;
        unit_21[2] /= relative_distance;

        spheres2.f_atr = spheres2.f_point;
        spheres2.f_rep = 0.0;
        spheres2.f_mag = spheres2.f_point;

        spheres2.F_ext[0] = unit_21[0] * spheres2.f_point;
        spheres2.F_ext[1] = unit_21[1] * spheres2.f_point;
        spheres2.F_ext[2] = unit_21[2] * spheres2.f_point;

        spheres2.M_ext[0] = 0.0;
        spheres2.M_ext[1] = 0.0;
        spheres2.M_ext[2] = 0.0;

        return detected_collision;
    }

    //#pragma omp parallel for reduction(+: f_0,f_1,f_2) schedule(static)
    for (size_t i = 0; i < spheres1.Np; i++)
    {
        for (size_t j = 0; j < spheres2.Np; j++)
        {
            unit_21[0] = spheres1.list[i].x - spheres2.list[j].x;
            unit_21[1] = spheres1.list[i].y - spheres2.list[j].y;
            unit_21[2] = spheres1.list[i].z - spheres2.list[j].z;

            dist = Vector_norm(unit_21[0], unit_21[1], unit_21[2]);

            double rc = (spheres1.list[i].Rp + spheres2.list[j].Rp) * factor_vdw;

            // Store minimum distance
            if (dist < d_ij_min)
            {
                d_ij_min = dist;
            }
            if (dist < rc)
            {
                detected_collision = true;
                /*
                std::cout << "Collision detected (moving,static): ("
                          << j << " " << i << "); dist/(R_p1+R_p2)= "
                          << dist / (spheres1.list[i].Rp + spheres2.list[j].Rp) 
                          << " factor_vdw: " << factor_vdw << std::endl;
                */
            }

            // Unit vector pointing from spheres2 (j) to spheres1 (i)
            unit_21[0] /= dist;
            unit_21[1] /= dist;
            unit_21[2] /= dist;

            // Interaction potential - attractive part
            //spheres2.list[j].f_atr = Attractive_point(dist, spheres2.list[j].Rp, A);
            spheres2.list[j].f_atr = Attractive_interaction(dist,
                                                            spheres1.list[i].Rp,
                                                            spheres2.list[j].Rp,
                                                            A);

            // Interaction potential - repulsive part
            double f_rep_max = Repulsive_interaction(rc,
                                                    spheres1.list[i].Rp,
                                                    spheres1.list[j].Rp,
                                                    A,
                                                    s_LJ);
            spheres2.list[j].f_rep = std::min(Repulsive_interaction(dist,
                                                                    spheres1.list[i].Rp,
                                                                    spheres1.list[j].Rp,
                                                                    A, s_LJ), f_rep_max);

            // Interaction potential - net resulting force
            spheres2.list[j].f_mag = spheres2.list[j].f_atr - spheres2.list[j].f_rep;

            if (spheres2.list[j].f_mag < 0.)
            {
                f_tot_21 = -spheres2.list[j].f_mag;
                unit_21[0] *= -1.0;
                unit_21[1] *= -1.0;
                unit_21[2] *= -1.0;
            }
            else
            {
                f_tot_21 = spheres2.list[j].f_mag;
            }

            f_tot_21 = spheres2.list[j].f_mag;

            f_atr += spheres2.list[j].f_atr;
            f_rep += spheres2.list[j].f_rep;

            // Net force
            f_0 += f_tot_21 * unit_21[0];
            f_1 += f_tot_21 * unit_21[1];
            f_2 += f_tot_21 * unit_21[2];

            // Net torque M_21
            d_j_cm2[0] = spheres2.list[j].x - spheres2.x_cm;
            d_j_cm2[1] = spheres2.list[j].y - spheres2.y_cm;
            d_j_cm2[2] = spheres2.list[j].z - spheres2.z_cm;
            f_21[0] = unit_21[0] * f_tot_21;
            f_21[1] = unit_21[1] * f_tot_21;
            f_21[2] = unit_21[2] * f_tot_21;
            Cross_product(d_j_cm2, f_21, M_21);
            m_0 += M_21[0];
            m_1 += M_21[1];
            m_2 += M_21[2];
        }
    }

    spheres2.f_atr = f_atr;
    spheres2.f_rep = f_rep;
    spheres2.f_mag = Vector_norm(f_0, f_1, f_2);

    spheres2.F_ext[0] = f_0;
    spheres2.F_ext[1] = f_1;
    spheres2.F_ext[2] = f_2;

    spheres2.M_ext[0] = m_0;
    spheres2.M_ext[1] = m_1;
    spheres2.M_ext[2] = m_2;

    return detected_collision;
}

/********************************************************************************
 * Function to determine the relative distance between particles
 ********************************************************************************/
double FTP::Relative_distance()
{
    double xt = spheres1.x_cm - spheres2.x_cm;
    double yt = spheres1.y_cm - spheres2.y_cm;
    double zt = spheres1.z_cm - spheres2.z_cm;
    spheres2.relative_distance = Vector_norm(xt, yt, zt);
    return spheres2.relative_distance;
}

/********************************************************************************
 * Function to calculate time step
 ********************************************************************************/
double FTP::Determine_time_step(Model model)
{
    // A logistic function is used to optimize the time step
    rc_agg = spheres1.Rmax + spheres2.Rmax;

    relative_distance = Relative_distance();
    double x0 = rc_agg * 2.;
    double k = 5 * x0;
    double lp_max = model.alpha_dp_max * spheres2.mean_Rp * 2.;
    double lp_min = model.alpha_dp * spheres2.mean_Rp * 2.;
    double lp = lp_max/(1.0 + std::exp(-k*(relative_distance-x0))) + lp_min;
    //double lp =  model.alpha_dp * spheres2.mean_Rp * 2.;

    double temp10 = std::pow(lp, 2) / (6. * spheres2.Di * spheres2.tau);
    double dt_1 = spheres2.tau * std::sqrt(temp10 * (temp10 + 2.));

    double f_grav = spheres2.m * (9.81);
    double dt_2 = 2 * dt_1;
    if (spheres2.f_mag > f_grav * (1e-06))
    {
        dt_2 = spheres2.f * lp / spheres2.f_mag;
    }

    // Below this limit LD is numerically unstable
    double t_limit = (1e-05) * spheres2.tau;
    return std::max(std::min(dt_1, dt_2), t_limit);
}
double FTP::Determine_time_step_rel_LD(Model model)
{
    // A logistic function is used to optimize the time step
    double tau_ij = spheres1.tau * spheres2.tau /(spheres1.tau + spheres2.tau);
    double D_ij = spheres1.Di * spheres2.Di /(spheres1.Di + spheres2.Di);
    rc_agg = spheres1.Rmax + spheres2.Rmax;

    relative_distance = Relative_distance();
    double x0 = rc_agg * 2.;
    double k = 5 * x0;
    double lp_max = model.alpha_dp_max * spheres2.mean_Rp * 2.;
    double lp_min = model.alpha_dp * spheres2.mean_Rp * 2.;
    double lp = lp_max/(1.0 + std::exp(-k*(relative_distance-x0))) + lp_min;
    //double lp =  model.alpha_dp * spheres2.mean_Rp * 2.;

    double temp10 = std::pow(lp, 2) / (6. * D_ij * tau_ij);
    double dt_1 = tau_ij * std::sqrt(temp10 * (temp10 + 2.));

    double f_grav = m_ij * (9.81);
    double dt_2 = 2 * dt_1;
    if (spheres2.f_mag > f_grav * (1e-06))
    {
        dt_2 = f_ij * lp / (2. * spheres2.f_mag);
    }

    // Below this limit LD is numerically unstable
    double t_limit = (1e-05) * tau_ij;
    return std::max(std::min(dt_1, dt_2), t_limit);
}

void FTP::One_rw_step_LD_method2(Model model)
{
    // Time step
    d_time = Determine_time_step(model);

    double dr[3];
    // Calculate interaction potential force
    collision = Interaction_potentials(model.A, model.s_LJ, model.factor_vdw, model.vdw_cutoff);

    double dv_br, dv;
    for (size_t k = 0; k < 3; k++)
    {
        dv_br = std::sqrt(6. * k_B * model.T_g * spheres2.f * d_time) * Gaussian(0., 1.)/spheres2.m;

        dr[k] = spheres2.v[k] * d_time;
        dv = (spheres2.F_ext[k] - spheres2.f * spheres2.v[k])* d_time/spheres2.m + dv_br;
        spheres2.v[k] += dv;
    }

    spheres2.x_ += dr[0];
    spheres2.y_ += dr[1];
    spheres2.z_ += dr[2];

    spheres2.x_cm += dr[0];
    spheres2.y_cm += dr[1];
    spheres2.z_cm += dr[2];
    for (size_t i = 0; i < spheres2.Np; i++)
    {
        for (size_t k = 0; k < 3; k++)
        {
            spheres2.list[i].v[k] = spheres2.v[k];
        }
        spheres2.list[i].x_ += dr[0];
        spheres2.list[i].y_ += dr[1];
        spheres2.list[i].z_ += dr[2];

        spheres2.list[i].x += dr[0];
        spheres2.list[i].y += dr[1];
        spheres2.list[i].z += dr[2];
    }

    spheres2.d_mag = Vector_norm(dr[0], dr[1], dr[2]);

    spheres2.l0 = dr[0] / spheres2.d_mag;
    spheres2.l1 = dr[1] / spheres2.d_mag;
    spheres2.l2 = dr[2] / spheres2.d_mag;

    if ((collision) && (!model.with_sticking))
    {
        spheres2.d_mag *= (-1);
        spheres2.v[0] *= (-1 / 1000);
        spheres2.v[1] *= (-1 / 1000);
        spheres2.v[2] *= (-1 / 1000);
        collision = false;
    }
    return;
    
}
/********************************************************************************
 * One step of the random walk using Langevin Dynamics
 ********************************************************************************/
void FTP::One_rw_step_LD(Model model)
{
    // Time step
    d_time = Determine_time_step(model);

    double dr[3];
    // Calculate interaction potential force
    collision = Interaction_potentials(model.A, model.s_LJ, model.factor_vdw, model.vdw_cutoff);

    // LANGEVIN DYNAMICS
    double v_new[3];
    if (model.with_brownian)
    {
        spheres2.new_VR_ran(model, d_time);
    }
    else
    {
        spheres2.V_ran[0] = spheres2.V_ran[1] = spheres2.V_ran[2] = 0.0;
        spheres2.R_ran[0] = spheres2.R_ran[1] = spheres2.R_ran[2] = 0.0;
    }

    for (size_t k = 0; k < 3; k++)
    {
        v_new[k] = spheres2.v[k] * spheres2.exp1 +
                    (spheres2.F_ext[k] / (spheres2.alpha * spheres2.m)) *
                    (1.0 - spheres2.exp1) + spheres2.V_ran[k];

        dr[k] = (1.0 / spheres2.alpha) * (v_new[k] + spheres2.v[k]
                - 2.0 * spheres2.F_ext[k] / (spheres2.alpha * spheres2.m)) * spheres2.r1 +
                (spheres2.F_ext[k] / (spheres2.alpha * spheres2.m)) * d_time + spheres2.R_ran[k];

        spheres2.v[k] = v_new[k];
    }

    spheres2.x_ += dr[0];
    spheres2.y_ += dr[1];
    spheres2.z_ += dr[2];

    spheres2.x_cm += dr[0];
    spheres2.y_cm += dr[1];
    spheres2.z_cm += dr[2];
    for (size_t i = 0; i < spheres2.Np; i++)
    {
        for (size_t k = 0; k < 3; k++)
        {
            spheres2.list[i].v[k] = spheres2.v[k];
        }
        spheres2.list[i].x_ += dr[0];
        spheres2.list[i].y_ += dr[1];
        spheres2.list[i].z_ += dr[2];

        spheres2.list[i].x += dr[0];
        spheres2.list[i].y += dr[1];
        spheres2.list[i].z += dr[2];
    }

    spheres2.d_mag = Vector_norm(dr[0], dr[1], dr[2]);

    spheres2.l0 = dr[0] / spheres2.d_mag;
    spheres2.l1 = dr[1] / spheres2.d_mag;
    spheres2.l2 = dr[2] / spheres2.d_mag;

    if ((collision) && (!model.with_sticking))
    {
        spheres2.d_mag *= (-1);
        spheres2.v[0] *= (-1 / 1000);
        spheres2.v[1] *= (-1 / 1000);
        spheres2.v[2] *= (-1 / 1000);
        collision = false;
    }
    return;
}

void FTP::One_rw_step_LD_rel_motion(Model model)
{
    // Time step
    d_time = Determine_time_step(model);

    double dr[3];
    // Calculate interaction potential force
    collision = Interaction_potentials(model.A, model.s_LJ, model.factor_vdw, model.vdw_cutoff);

    if(collision)
    {
        return;
    }

    // LANGEVIN DYNAMICS
    double v_new[3];
    // Dimensionless parameters
    double exp1_sph1 = std::exp(-spheres1.alpha * d_time);
    double exp1_sph2 = std::exp(-spheres2.alpha * d_time);
    double r1_sph1 = (1.0 - exp1_sph1)/(1.0 + exp1_sph1);
    double r1_sph2 = (1.0 - exp1_sph2)/(1.0 + exp1_sph2);
    
    double w_12 = (spheres1.m * exp1_sph2 + spheres2.m * exp1_sph1)/(spheres1.m + spheres2.m);
    double x_12 = (spheres1.f * exp1_sph2 + spheres2.f * exp1_sph1)/(spheres1.f * spheres2.f);
    double k_1 = r1_sph1/spheres1.alpha;
    double k_2 = r1_sph2/spheres2.alpha;
    double k_12 = (k_1 * spheres2.m + k_2 * spheres1.m)/(spheres1.m + spheres2.m);
    double k_12_p = (k_1 / spheres1.f + k_2 / spheres2.f);

    double f_12 = (spheres1.f * spheres2.f)/(spheres1.f + spheres2.f);
    double a_12 = spheres1.Rs + spheres2.Rs;
    double m_12 = (spheres1.m * spheres2.m)/(spheres1.m + spheres2.m);

    m_ij = m_12;
    f_ij = f_12;
    Kn_d_ij = std::sqrt(k_B * model.T_g * m_12)/(f_12*a_12);

    if (model.with_brownian)
    {
        new_VR_random_relative(model, exp1_sph1, exp1_sph2, r1_sph1, r1_sph2);
    }
    else
    {
        spheres2.V_ran[0] = spheres2.V_ran[1] = spheres2.V_ran[2] = 0.0;
        spheres2.R_ran[0] = spheres2.R_ran[1] = spheres2.R_ran[2] = 0.0;
    }

    for (size_t k = 0; k < 3; k++)
    {
        v_new[k] = spheres2.v[k] * w_12 +
                   spheres2.F_ext[k] * (std::pow(f_12,-1) - x_12) +
                   spheres2.V_ran[k];

        dr[k] = (v_new[k] + spheres2.v[k]) * k_12 -
                2. * spheres2.F_ext[k] * k_12_p +
                spheres2.F_ext[k] * std::pow(f_12,-1) * d_time +
                spheres2.R_ran[k];

        spheres2.v[k] = v_new[k];
    }

    spheres2.x_ += dr[0];
    spheres2.y_ += dr[1];
    spheres2.z_ += dr[2];

    //spheres2.x_cm += dr[0];
    //spheres2.y_cm += dr[1];
    //spheres2.z_cm += dr[2];
    for (size_t i = 0; i < spheres2.Np; i++)
    {
        for (size_t k = 0; k < 3; k++)
        {
            spheres2.list[i].v[k] = spheres2.v[k];
        }
        spheres2.list[i].x_ += dr[0];
        spheres2.list[i].y_ += dr[1];
        spheres2.list[i].z_ += dr[2];

        //spheres2.list[i].x += dr[0];
        //spheres2.list[i].y += dr[1];
        //spheres2.list[i].z += dr[2];
    }

    spheres2.d_mag = Vector_norm(dr[0], dr[1], dr[2]);

    spheres2.l0 = dr[0] / spheres2.d_mag;
    spheres2.l1 = dr[1] / spheres2.d_mag;
    spheres2.l2 = dr[2] / spheres2.d_mag;

    Collisions(model.factor_vdw);
    Effective_displacement();

    // If modeling without sticking upon collisions -> rebound
    if ((collision) && (!model.with_sticking))
    {
        spheres2.d_mag *= (-1);
        spheres2.v[0] *= (-1 / 1000);
        spheres2.v[1] *= (-1 / 1000);
        spheres2.v[2] *= (-1 / 1000);
        collision = false;
        std::cout << " Rebound " << std::endl;
    }
}

void FTP::new_VR_random_relative(const Model model,
                                 const double exp1_sph1,
                                 const double exp1_sph2,
                                 const double r1_sph1,
                                 const double r1_sph2)
{

    double exp2_sph1 = std::exp(-2.0 * spheres1.alpha * d_time);
    double exp2_sph2 = std::exp(-2.0 * spheres2.alpha * d_time);

    double B1_sq = std::sqrt((k_B * model.T_g / spheres1.m) * (1 - exp2_sph1)+\
                             (k_B * model.T_g / spheres2.m) * (1 - exp2_sph2));
    double B2_sq = std::sqrt((2.0 * k_B * model.T_g / spheres1.m) *
                      (1.0 / std::pow(spheres1.alpha, 2)) *
                      (spheres1.alpha * d_time - 2.0 * r1_sph1)+
                             (2.0 * k_B * model.T_g / spheres2.m) *
                      (1.0 / std::pow(spheres2.alpha, 2)) *
                      (spheres2.alpha * d_time - 2.0 * r1_sph2));

    for (size_t k = 0; k < 3; k++)
    {
        spheres2.V_ran[k] = B1_sq * Gaussian(0.,1.);
        spheres2.R_ran[k] = B2_sq * Gaussian(0.,1.);
    }
}

bool FTP::Multi_rw_step_LD(Model model,
                           const size_t n_steps)
{
    for (size_t i = 0; i < n_steps; i++)
    {
        // An individual RW step
        if(model.with_rel_motion)
        {
            One_rw_step_LD_rel_motion(model);
        } else
        {
            //One_rw_step_LD_method2(model);
            One_rw_step_LD(model);
            if(model.with_rotation)
            {
                One_rw_step_rotation_LD(model);
            } else {
                spheres2.v_rot[0]=spheres2.v_rot[1]=spheres2.v_rot[2]=0.0;
            }
        }

        t_res += d_time;

        if (collision)
        {
            return collision;
        }

        // Periodic Boundary Conditions
        Periodic_boundary_condition();
    }
    return collision;
}

/********************************************************************************
 * Rotational motion
 ********************************************************************************/
void FTP::One_rw_step_rotation_LD(Model model)
{
    double dw_br;
    double dv_rot;

    for (size_t k = 0; k < 3; k++)
    {
        if (model.with_brownian)
        {
            dw_br = std::sqrt(2 * k_B * model.T_g * spheres2.f_rot * d_time / 3.0) * Gaussian(0., 1.) / spheres2.inertia_moment[k];
        }
        else
        {
            dw_br = 0.0;
        }

        spheres2.n_or[k] += spheres2.v_rot[k] * d_time;
        dv_rot = (spheres2.M_ext[k] - spheres2.f_rot * spheres2.v_rot[k]) * d_time / spheres2.inertia_moment[k] + dw_br;
        spheres2.v_rot[k] += dv_rot;
    }
    spheres2.rotate_agglomerate_axes();
}

/********************************************************************************
 * Move CM to origin
 ********************************************************************************/
void FTP::Move_to_origin()
{
    // MOVE AGGREGATE CENTER OF MASS TO ORIGIN
    std::array<double, 3> center_mass = {spheres1.x_cm,
                                         spheres1.y_cm,
                                         spheres1.z_cm};
    for (size_t j = 0; j < spheres1.Np; j++)
    {
        spheres1.list[j].x -= center_mass[0];
        spheres1.list[j].y -= center_mass[1];
        spheres1.list[j].z -= center_mass[2];
    };
    spheres1.x_cm -= center_mass[0];
    spheres1.y_cm -= center_mass[1];
    spheres1.z_cm -= center_mass[2];
}

/********************************************************************************
 * Random initial location
 ********************************************************************************/
void FTP::Random_initial_location()
{
    size_t rand_side = rand() % 6;
    double x_rand = L_box * (rand_01() - 0.5);
    double y_rand = L_box * (rand_01() - 0.5);
    std::array<double, 3> new_location = {0., 0., 0.};
    /*
    switch (rand_side)
    {
    case 0:
        new_location = {-L_box / 2., x_rand, y_rand};
    case 1:
        new_location = {L_box / 2., x_rand, y_rand};
    case 2:
        new_location = {x_rand, -L_box / 2., y_rand};
    case 3:
        new_location = {x_rand, L_box / 2., y_rand};
    case 4:
        new_location = {x_rand, y_rand, -L_box / 2.};
    case 5:
        new_location = {x_rand, y_rand, L_box / 2.};
    }
    */
    // random point in a sphere
    std::array<double, 3> point;
    spheres2.Rand_point_sphere(point);
    new_location = {L_box * point[0]/ 2.,
                    L_box * point[1]/ 2.,
                    L_box * point[2]/ 2.};
    std::array<double, 3> center_mass = {spheres2.x_cm,
                                         spheres2.y_cm,
                                         spheres2.z_cm};
    for (size_t j = 0; j < spheres2.Np; j++)
    {
        spheres2.list[j].x += new_location[0] - center_mass[0];
        spheres2.list[j].y += new_location[1] - center_mass[1];
        spheres2.list[j].z += new_location[2] - center_mass[2];

        spheres2.list[j].x_ = spheres2.list[j].x;
        spheres2.list[j].y_ = spheres2.list[j].y;
        spheres2.list[j].z_ = spheres2.list[j].z;
    };
    spheres2.x_cm += new_location[0] - center_mass[0];
    spheres2.y_cm += new_location[1] - center_mass[1];
    spheres2.z_cm += new_location[2] - center_mass[2];

    spheres2.x_ = spheres2.x_cm;
    spheres2.y_ = spheres2.y_cm;
    spheres2.z_ = spheres2.z_cm;
}

/********************************************************************************
 * Periodic boundary conditions asuming a cubic box
 ********************************************************************************/
void FTP::Periodic_boundary_condition()
{
    double displacement[3] = {0., 0., 0.};
    if (spheres2.x_cm < -0.5 * L_box)
    {
        displacement[0] = L_box;
    }
    else if (spheres2.x_cm > 0.5 * L_box)
    {
        displacement[0] = -L_box;
    }

    if (spheres2.y_cm < -0.5 * L_box)
    {
        displacement[1] = L_box;
    }
    else if (spheres2.y_cm > 0.5 * L_box)
    {
        displacement[1] = -L_box;
    }

    if (spheres2.z_cm < -0.5 * L_box)
    {
        displacement[2] = L_box;
    }
    else if (spheres2.z_cm > 0.5 * L_box)
    {
        displacement[2] = -L_box;
    }
    Aggregate_displacement(displacement);

    // Check and discard errors
    if (spheres2.x_cm < -0.5000001 * L_box || spheres2.x_cm > 0.5000001 * L_box)
    {
        std::cout << " error, particle out of the simulation domain (x) " << spheres2.x_cm << " L_box " << L_box << " disp " << displacement[0] << std::endl;
        std::cout << " relative_distance " << relative_distance << std::endl;
        exit(EXIT_FAILURE);
    }

    if (spheres2.y_cm < -0.5000001 * L_box || spheres2.y_cm > 0.5000001 * L_box)
    {
        std::cout << " error, particle out of the simulation domain (y) " << spheres2.y_cm << " L_box " << L_box << " disp " << displacement[1] << std::endl;
        std::cout << " relative_distance " << relative_distance << std::endl;
        exit(EXIT_FAILURE);
    }

    if (spheres2.z_cm < -0.5000001 * L_box || spheres2.z_cm > 0.5000001 * L_box)
    {
        std::cout << " error, particle out of the simulation domain (z) " << spheres2.z_cm << " L_box " << L_box << " disp " << displacement[2] << std::endl;
        std::cout << " relative_distance " << relative_distance << std::endl;
        exit(EXIT_FAILURE);
    }
}

/********************************************************************************
 * Aggregate and particle array object are displaced
 ********************************************************************************/
void FTP::Aggregate_displacement(const double displacement[3])
{
    spheres2.x_cm += displacement[0];
    spheres2.y_cm += displacement[1];
    spheres2.z_cm += displacement[2];

    spheres2.x_ += displacement[0];
    spheres2.y_ += displacement[1];
    spheres2.z_ += displacement[2];

    for (size_t i = 0; i < spheres2.Np; i++)
    {
        spheres2.list[i].x_ += displacement[0];
        spheres2.list[i].y_ += displacement[1];
        spheres2.list[i].z_ += displacement[2];

        spheres2.list[i].x += displacement[0];
        spheres2.list[i].y += displacement[1];
        spheres2.list[i].z += displacement[2];
    }
}

/********************************************************************************
 * Check for collisions with monomers belonging to the growing aggregate
 ********************************************************************************/
void FTP::Collisions(const double factor_vdw)
{
    double dist_min(10000000.0);
    // int min(-1);

    spheres2.displac_collision = spheres2.d_mag;

    // Improve CPU time -> consider the aggregate as a sphere or radius rmax
    rc_agg = spheres1.Rmax + spheres2.Rmax;
    double xt = spheres1.x_cm - spheres2.x_;
    double yt = spheres1.y_cm - spheres2.y_;
    double zt = spheres1.z_cm - spheres2.z_;

    spheres2.relative_distance = std::sqrt((xt * xt) + (yt * yt) + (zt * zt));
    relative_distance = spheres2.relative_distance;

    for (size_t i = 0; i < spheres2.Np; i++)
    {
        for (size_t j = 0; j < spheres1.Np; j++)
        {
            // Line-sphere intersection: https://en.wikipedia.org/wiki/Line%E2%80%93sphere_intersection
            // Sphere radius r=rc
            double rc = (spheres1.list[j].Rp + spheres2.list[i].Rp) * factor_vdw;
            // Sphere center to origin distance o-c =(xt,yt,zt)
            double xt = spheres2.list[i].x - spheres1.list[j].x;
            double yt = spheres2.list[i].y - spheres1.list[j].y;
            double zt = spheres2.list[i].z - spheres1.list[j].z;

            // Dot product u*(o-c), where u=(l0,l1,l2)
            double l_c = spheres2.l0 * xt + spheres2.l1 * yt + spheres2.l2 * zt;
            // c_2 = (o-c)^2
            double c_2 = (xt * xt) + (yt * yt) + (zt * zt);
            // root=discriminant of the quadratic equation
            double root = (l_c * l_c) - c_2 + (rc * rc); // remember (l0,l1,l2) is unitary

            // intersection
            if (root > 0)
            {
                double dist1 = -l_c + std::sqrt(root);
                double dist2 = -l_c - std::sqrt(root);
                double dist = std::max(dist1, dist2);
                if ((dist > 0) && (dist < spheres2.d_mag))
                {
                    // min = int(j); //the one we collide first
                    collision = true;
                    //std::cout << "Collision detected (moving,static): ("
                    //          << i << " " << j << "); dist= " << dist << std::endl;

                    // Check for the minimum distance (first collision)
                    if (dist < dist_min)
                    {
                        dist_min = dist;
                        spheres2.displac_collision = dist;
                    }
                };
            };
        }
    }
}

/********************************************************************************
 * When no collision, agglomerate is displaced
 ********************************************************************************/
void FTP::Effective_displacement()
{
    double displacement = spheres2.displac_collision;
    for (size_t i = 0; i < spheres2.Np; i++)
    {
        spheres2.list[i].x += spheres2.l0 * displacement;
        spheres2.list[i].y += spheres2.l1 * displacement;
        spheres2.list[i].z += spheres2.l2 * displacement;
    }
    spheres2.x_cm += spheres2.l0 * displacement;
    spheres2.y_cm += spheres2.l1 * displacement;
    spheres2.z_cm += spheres2.l2 * displacement;
}

/********************************************************************************
 * Generate a random Gaussian distributed number
 ********************************************************************************/
double FTP::Gaussian(double mu, double sigma)
{
    static double sqrt2logx1, twopix2;

    if (first)
    {
        first = false;
        sqrt2logx1 = std::sqrt(-2. * std::log(drand48()));
        twopix2 = 2. * PI * drand48();
        return mu + sigma * sqrt2logx1 * std::cos(twopix2);
    }
    else
    {
        first = true;
        return mu + sigma * sqrt2logx1 * std::sin(twopix2);
    }
}
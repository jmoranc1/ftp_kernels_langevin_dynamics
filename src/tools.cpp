/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tools.hpp"

using namespace std;

/********************************************************************************
 * Make a folder to export results
 ********************************************************************************/
void create_output_folder()
{
    ostringstream fname;
    fname << "rm -r data_out" << endl;
    system(fname.str().c_str());
    fname.str(""); // empty fname
    fname << "mkdir data_out" << endl;
    system(fname.str().c_str());
    fname.str(""); // empty fname
}

/********************************************************************************
 * write particle position to text file
 ********************************************************************************/
void out_pos(std::vector<particle> p, size_t counter, size_t Np)
{
    FILE *pFile;
    char buffer[50];
    int precision = 10;
    std::string fpath;
    fpath += "data_out/LD_";
    gcvt(Np, precision, buffer);
    fpath += buffer;
    fpath += "_";
    gcvt(counter, precision, buffer);
    fpath += buffer;
    fpath += ".dat";
    char *pch2 = (char *)malloc(sizeof(char) * (fpath.length() + 1));
    std::string::traits_type::copy(pch2, fpath.c_str(), fpath.length() + 1);
    pFile = fopen(pch2, "w");
    if (pFile == NULL)
        perror("Error opening file");
    else
    {
        for (size_t i = 0; i < Np; i++)
        {
            fprintf(pFile, "%le %le %le %le \n", p[i].x, p[i].y, p[i].z, p[i].Rp);
        }
        fclose(pFile);
    }
    return;
}

/********************************************************************************
 * get the directory of aggregates
 * https://www.linuxquestions.org/questions/programming-9/c-list-files-in-directory-379323/
 ********************************************************************************/
size_t getdir(std::string dir, vector<std::string> &files)
{
    size_t count(0);
    DIR *dp;
    struct dirent *dirp;
    if ((dp = opendir(dir.c_str())) == NULL)
    {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL)
    {
        if (!strcmp(dirp->d_name, ".") || !strcmp(dirp->d_name, ".."))
        {
            // do nothing (straight logic)
        }
        else
        {
            files.push_back(std::string(dirp->d_name));
            // cout << "file: "<<count<<" "<< string(dirp->d_name) << endl;
            count++;
        };
    }
    closedir(dp);
    return count;
}
vector<std::string> getdir_files(std::string dir)
{
    vector<std::string> files;
    //size_t count(0);
    DIR *dp;
    struct dirent *dirp;
    if ((dp = opendir(dir.c_str())) == NULL)
    {
        std::cout << "Error(" << errno << ") opening " << dir << std::endl;
        return files;
    }

    while ((dirp = readdir(dp)) != NULL)
    {
        if (!strcmp(dirp->d_name, ".") || !strcmp(dirp->d_name, ".."))
        {
            // do nothing (straight logic)
        }
        else
        {
            files.push_back(std::string(dirp->d_name));
            // cout << "file: "<<count<<" "<< string(dirp->d_name) << endl;
        };
    }
    closedir(dp);
    return files;
}

/********************************************************************************
 * Obtain the number of lines in an external file
 ********************************************************************************/
int number_lines(std::string dir)
{
    int number_of_lines = 0;
    std::string line;
    std::ifstream file(dir);

    while (std::getline(file, line))
    {
        ++number_of_lines;
    }
    return number_of_lines;
}

particle_list load_agg_external_ifstream(const std::string dir,
                                         const Model model)
{
    particle_list spheres;

    std::vector<particle> p;
    std::vector<double> x,y,z,r;
    double mean_rp(0.0);
    size_t counter(0);

    //std::cout << "Load aggregate (ifstream) from: " << dir << std::endl;

    std::ifstream ext_FILE;
    ext_FILE.open(dir, std::ifstream::in);

    int n_lines = number_lines(dir);

    std::string line, value;
    for (size_t i = 0; i < n_lines; i++)
    {
        array<double, 4> current_number; // WE ASUME: External agg files are: [x y z Rp]
        for (size_t j = 0; j < 4; j++)
        {
            ext_FILE >> current_number[j];
            //std::cout << current_number[j] << " ";
        }
        //std::cout << std::endl;
        x.push_back(current_number[0]);
        y.push_back(current_number[1]);
        z.push_back(current_number[2]);
        r.push_back(current_number[3]);
        
        mean_rp += current_number[3];
        counter++;
    }

    mean_rp /= static_cast<double>(counter);
    double new_rp = model.D_pp * 0.5;

    // Initialize particles
    for (size_t j = 0; j < counter; j++)
    {
        p.push_back(particle(x[j] * new_rp / mean_rp,
                             y[j] * new_rp / mean_rp,
                             z[j] * new_rp / mean_rp,
                             r[j] * new_rp / mean_rp,
                             model));
    }

    spheres.Set_particle_list(p, model);

    ext_FILE.close();
    return spheres;
}

/********************************************************************************
 * update agg and p according to external aggregates
 ********************************************************************************/
particle_list load_agg_external_getline(const std::string dir,
                                        const Model model)
{
    particle_list spheres;
    // number of lines in the file
    std::ifstream file(dir);
    if (!file.is_open())
    {
        std::cout << "Failed to read input file: " << dir << std::endl;
        return spheres;
    }

    std::vector<particle> p;
    std::vector<double> x,y,z,r;
    double mean_rp(0.0);
    size_t counter(0);

    std::cout << "Load aggregate (getline) from: " << dir << std::endl;

    std::string line, value;
    while (std::getline(file, line))
    {
        std::stringstream ss(line);
        array<double, 4> current_number; // WE ASUME: External agg files are: [x y z Rp]
        for (size_t j = 0; j < 4; j++)
        {
            if(Check_commas(line))
            {
                std::getline(ss, value, ',');
                current_number[j] = std::stod(value);
            } else {
                std::getline(ss, value, '\t');
                current_number[j] = std::stod(value);
            }
            std::cout << current_number[j] << " ";
        }
        std::cout << std::endl;
        x.push_back(current_number[0]);
        y.push_back(current_number[1]);
        z.push_back(current_number[2]);
        r.push_back(current_number[3]);
        
        mean_rp += current_number[3];
        counter++;
    }

    mean_rp /= static_cast<double>(counter);
    double new_rp = model.D_pp * 0.5;

    // Initialize particles
    for (size_t j = 0; j < counter; j++)
    {
        p.push_back(particle(x[j] * new_rp / mean_rp,
                             y[j] * new_rp / mean_rp,
                             z[j] * new_rp / mean_rp,
                             r[j] * new_rp / mean_rp,
                             model));
    }

    spheres.Set_particle_list(p, model);

    // Close the file.
    file.close();
    return spheres;
}
bool Check_commas(std::string line)
{
    for (auto x : line)
    {
        if (x == ',')
        {
            return true;
        }
    }
    return false;
}

void load_agg_external(const std::string dir,
                       const Model model,
                       particle_list &spheres)
{
    //spheres.Copy(load_agg_external_getline(dir, model), model);
    spheres.Copy(load_agg_external_ifstream(dir, model), model);
}

/********************************************************************************
 * Lognormally distributed random monomers
 ********************************************************************************/
__attribute((const)) double inverfc(const double pp)
{
    double x, t, ppp;
    if (pp >= 2.0)
    {
        return -100.;
    }
    if (pp <= 0.0)
    {
        return 100.;
    }
    ppp = (pp < 1.0) ? pp : 2. - pp;
    t = std::sqrt(-2. * std::log(ppp / 2.));
    x = -0.70711 * ((2.30753 + t * 0.27061) / (1. + t * (0.99229 + t * 0.04481)) - t);
    for (int j = 0; j < 2; j++)
    {
        double err = erfc(x) - ppp;
        x += err / (1.12837916709551257 * exp(-(x * x)) - x * err);
    }
    return (pp < 1.0 ? x : -x);
}

__attribute((const)) double inverf(const double pp) { return inverfc(1. - pp); };

double Get_Lognormal_rand(const double Dp, const double Dp_sigmaG)
{
    if (Dp_sigmaG > 1.0)
    {
        double x = rand_01();
        return 0.5 * std::exp(std::log(Dp) + std::sqrt(2.0) * std::log(Dp_sigmaG) * inverf(2.0 * x - 1.0)); // Log-normal
    }
    else if (Dp_sigmaG == 1.0)
    {
        return 0.5 * Dp;
    }
    else
    {
        cout << " error, Dp_sigmaG cannot be lower than 1" << endl;
        exit(EXIT_FAILURE);
    };
}

double Get_normal_rand(const double mean, const double sigma) {
    double rand_normal = mean + std::sqrt(2.) * sigma* inverf(2. * rand_01() - 1.0);
    return rand_normal;
}

/********************************************************************************
 * Seed (pseudo) random numbers according to CPU time
 ********************************************************************************/
void InitRandom()
{
    time_t t;
    time(&t);
    srand(uint(t));
    // srand(0);
}

/********************************************************************************
 * Random Gaussian numbers following the Box-Muller method
 ********************************************************************************/
void Get_rand_gauss(double (&Y)[6])
{
    double temp, temp2;
    double sigma = 1.;
    double num[6];

    //! Box and Muller method
    for (int q = 0; q < 6; q++)
    {
        num[q] = rand_01();
        if ((q + 1) % 2 == 0.)
        {
            temp = sigma * sqrt(-2. * log(num[q - 1]));
            temp2 = 2. * PI * num[q];

            Y[q - 1] = temp * cos(temp2);
            Y[q] = temp * sin(temp2);
        };
    };
}

/********************************************************************************
 * Random point in a sphere (Not biased, from Wolfram)
 ********************************************************************************/
void Rand_point_sphere(vector<double> &point)
{
    // -- Generating a random direction --
    double thetarandom = rand_01() * PI * 2;
    double phirandom = std::acos(1 - 2 * rand_01());
    point = {{std::sin(phirandom) * std::cos(thetarandom),
              std::sin(phirandom) * std::sin(thetarandom),
              std::cos(phirandom)}};
}

double Vector_norm(const double x,
                   const double y,
                   const double z)
{
    return std::sqrt(std::pow(x, 2) +
                     std::pow(y, 2) +
                     std::pow(z, 2));
}

/********************************************************************************
 * Print main physical properties on the screen
 ********************************************************************************/
void SUB_show_parameters(const particle_list spheres, const Model model)
{
    model.Show_properties();

    size_t npp = spheres.Np;
    double v_cr, v_th;
    v_cr = std::sqrt(model.A / (PI * model.Rho_p * Z * std::pow(2.0 * spheres.list[npp - 1].Rp, 2)));
    v_th = std::sqrt(8. * k_B * model.T_g / (PI * spheres.list[npp - 1].m));

    std::cout << "Rp (nm), Kn, Cc  " << std::endl;
    for (size_t i = 0; i < npp; i++)
    {
        std::cout << spheres.list[i].Rp * std::pow(10., 9.0) << " " << spheres.list[i].Kn << " " << spheres.list[i].Cc << std::endl;
    };
    std::cout << " " << std::endl;

    std::cout << "Properties of the last particle saved" << std::endl;
    std::cout << "Particle mfp (nm)       " << std::sqrt(18. * spheres.list[npp - 1].Di * spheres.list[npp - 1].tau) * std::pow(10., 9.0) << std::endl;
    std::cout << "Friction factor (N/s)   " << spheres.list[npp - 1].f << std::endl;
    std::cout << "Relaxation time (us)    " << spheres.list[npp - 1].tau * std::pow(10., 6.0) << std::endl;
    std::cout << "(lD dt)/(Relax. time)   " << spheres.list[npp - 1].d_time / spheres.list[npp - 1].tau << std::endl;
    std::cout << "Diffusion coeff (m^2/s) " << spheres.list[npp - 1].Di << std::endl;
    std::cout << "Maxwell avg vel (m/s)   " << v_th << std::endl;
    std::cout << "Critical vel v_cr (m/s) " << v_cr << std::endl;
    std::cout << "Critical vel. v_cr/v_th " << v_cr / v_th << std::endl;
    std::cout << "G, H, I                 " << spheres.list[npp - 1].G << " " << spheres.list[npp - 1].H << " " << spheres.list[npp - 1].I << std::endl;
    std::cout << "I-H^2/G                 " << spheres.list[npp - 1].I - std::pow(spheres.list[npp - 1].H, 2.0) / spheres.list[npp - 1].G << std::endl;
    std::cout << "LD d_time (ms)          " << spheres.list[npp - 1].d_time * std::pow(10., 6.0) << std::endl;
    std::cout << " " << std::endl;
}

/********************************************************************************
 * Cross product a x b = c
 ********************************************************************************/
void Cross_product(const double a[3],
                   const double b[3],
                   double (&c)[3])
{
    c[0] = a[1]*b[2] - a[2]*b[1];
    c[1] = a[2]*b[0] - a[0]*b[2];
    c[2] = a[0]*b[1] - a[1]*b[0];
}
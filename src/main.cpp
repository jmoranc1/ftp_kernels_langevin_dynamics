/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "main.hpp"

using namespace std;

FTP ftp_model;

/********************************************************************************
* Main program
********************************************************************************/
int main()
{
    Model model;
    model.Set_properties_default();

    // Initialize random numbers
    InitRandom();

    // Make folder for output data
    create_output_folder();

    // Path containing input aggregates
    std::vector<string> files;

    // Import external aggregate
    size_t N_files = getdir(model.path, files);

    // Iterate over N_files files
    for (size_t k = 0; k < N_files; k++)
    {
        // Set particle_lists
        std::string path_agg = model.path + "/" + files[k];
        particle_list spheres1, spheres2;
        load_agg_external(path_agg, model, spheres1);
        load_agg_external(path_agg, model, spheres2);

        ftp_model.Set_spherelist(spheres1,spheres2);
   
        std::cout << files[k] << "\n spheres1, spheres2 (N_p): " << spheres1.Np << " " << spheres2.Np << std::endl;

        if (k == 0)
        {
            SUB_show_parameters(spheres2, model);
        }

        // Move aggregare-1 to the origin
        ftp_model.Move_to_origin();

        double t_res_mean(.0);
        // Iterate a number of runs for statistics
        for (size_t j = 0; j < model.N_runs; j++)
        {
            // Random initial location for aggregate-2
            ftp_model.Random_initial_location();

            // Simulate the motion of aggregate-2 until collision with agg.-1
            bool collision = false;
            while (!collision)
            {
                collision = ftp_model.Multi_rw_step_LD(model, 1);

                std::cout << "t: " << ftp_model.t_res << ", dist:  " << spheres2.relative_distance/(ftp_model.L_box/2.) << std::endl;
            }
            t_res_mean += ftp_model.t_res;
        }

        t_res_mean /= static_cast<double>(model.N_runs);

        std::cout << "Cluster " << k + 1 << " done " << std::endl;
        std::cout << "   - Mean FTP time (ms) " << t_res_mean * pow(10., 3.0) << std::endl;

        //k_tmp = k + 1;
        //out_pos(p, k_tmp, Np);
    }
    return 0;
}

/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "particle.hpp"

using namespace std;

/********************************************************************************
 * Particle properties, like size, mass, etc...
 ********************************************************************************/
particle::particle() {}

particle::particle(double radius)
{
    Rp = radius;
    rmax = radius;
    x = y = z = 0.0;
    x_ = y_ = z_ = 0.0;
    l0 = l1 = l2 = 0.0;
    d_mag = 0.0;
    displac_collision = 0.0;
    relative_distance = 0.0;
    f_atr = f_rep = f_mag = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2] = 0.0;

    m = Kn = Cc = 0.0;
    f = Di = alpha = 0.0;
    d_time = tau = 0.0;

    G = 0.0;
    H = 0.0;
    I = 0.0;
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0;
    langevin_check = true;

    v[0] = v[1] = v[2] = 0.0;
    v_ini[0] = v_ini[1] = v_ini[2] = 0.0;
    c_rms = 0.0;
    t_res = 0.0;
    r_seed = 0.0;
    r_c = 2.0 * radius;
    rg = std::sqrt(3.0 / 5.0) * radius;
    df = 3.0;
    Np = 1;
    lambda_p = 0.0;
    f_rot = 0.0;
}

particle::particle(double radius, const Model model)
{
    Rp = radius;
    rmax = radius;
    x = y = z = 0.0;
    x_ = y_ = z_ = 0.0;
    l0 = l1 = l2 = 0.0;
    d_mag = 0.0;
    displac_collision = 0.0;
    relative_distance = 0.0;
    f_atr = f_rep = f_mag = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2] = 0.0;

    m = model.Rho_p * (4.0 * PI / 3.0) * (radius * radius * radius);
    Kn = model.lambda_g / radius;

    Cc = 1. + Kn * (A1_t + A2_t * std::exp(-A3_t / Kn));
    f = 3.0 * PI * model.mu_g * (2.0 * radius) / Cc;

    double Cc_r = 1. + Kn * (A1_r + A2_r * std::exp(-A3_r / Kn));
    f_rot = 8.0 * PI * model.mu_g * std::pow(radius, 3) / Cc_r;

    Di = k_B * model.T_g / f;
    alpha = f / m;
    d_time = model.LD_dt_factor * m / f; // pow((LD_dt_factor*(2.0*Rp)),2)/(2*Di);
    tau = m / f;

    G = (k_B * model.T_g / m) * (1. - std::exp(-2. * alpha * d_time));
    H = (k_B * model.T_g / m) * (1. / alpha) * std::pow((1. - std::exp(-alpha * d_time)), 2.0);
    I = (k_B * model.T_g / m) * (1. / std::pow(alpha, 2.0)) * (2. * alpha * d_time - 3. + 4. * std::exp(-alpha * d_time) - std::exp(-2. * alpha * d_time));
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0;
    langevin_check = true;

    v[0] = v[1] = v[2] = (k_B * model.T_g / m);
    v_ini[0] = v_ini[1] = v_ini[2] = (k_B * model.T_g / m);
    c_rms = std::sqrt(3.0 * k_B * model.T_g / m);
    t_res = 0.0;
    r_seed = 0.0;
    r_c = 2.0 * radius;
    rg = std::sqrt(3.0 / 5.0) * radius;
    df = 3.0;
    Np = 1;

    lambda_p = std::sqrt(18. * Di * tau);
}

particle::particle(double pos_x,
                   double pos_y,
                   double pos_z,
                   double radius,
                   const Model model)
{
    Rp = radius;
    rmax = radius;
    x = pos_x;
    y = pos_y;
    z = pos_z;

    x_ = pos_x;
    y_ = pos_y;
    z_ = pos_z;

    l0 = l1 = l2 = 0.0;
    d_mag = 0.0;
    displac_collision = 0.0;
    relative_distance = 0.0;
    f_atr = f_rep = f_mag = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2] = 0.0;

    m = model.Rho_p * (4.0 * PI / 3.0) * (radius * radius * radius);
    Kn = model.lambda_g / radius;
    Cc = 1. + Kn * (A1_t + A2_t * std::exp(-A3_t / Kn));
    f = 3.0 * PI * model.mu_g * (2.0 * radius) / Cc;

    double Cc_r = 1. + Kn * (A1_r + A2_r * std::exp(-A3_r / Kn));
    f_rot = 8.0 * PI * model.mu_g * std::pow(radius, 3) / Cc_r;

    Di = k_B * model.T_g / f;
    alpha = f / m;
    d_time = model.LD_dt_factor * m / f; // pow((LD_dt_factor*(2.0*Rp)),2)/(2*Di);
    tau = m / f;

    G = (k_B * model.T_g / m) * (1. - std::exp(-2. * alpha * d_time));
    H = (k_B * model.T_g / m) * (1. / alpha) * std::pow((1. - std::exp(-alpha * d_time)), 2.0);
    I = (k_B * model.T_g / m) * (1. / std::pow(alpha, 2.0)) * (2. * alpha * d_time - 3. + 4. * std::exp(-alpha * d_time) - std::exp(-2. * alpha * d_time));
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0;
    langevin_check = true;

    v[0] = v[1] = v[2] = (k_B * model.T_g / m);
    v_ini[0] = v_ini[1] = v_ini[2] = (k_B * model.T_g / m);
    c_rms = std::sqrt(3.0 * k_B * model.T_g / m);
    t_res = 0.0;
    r_seed = 0.0;
    r_c = 2.0 * radius;
    rg = std::sqrt(3.0 / 5.0) * radius;
    df = 3.0;
    Np = 1;

    lambda_p = std::sqrt(18. * Di * tau);
}

void particle::particle_restart(double radius,
                                const Model model)
{
    Rp = radius;
    rmax = radius;
    x = y = z = 0.0;
    x_ = y_ = z_ = 0.0;
    l0 = l1 = l2 = 0.0;
    d_mag = 0.0;
    displac_collision = 0.0;
    relative_distance = 0.0;
    f_atr = f_rep = f_mag = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2] = 0.0;

    m = model.Rho_p * (4.0 * PI / 3.0) * (radius * radius * radius);
    Kn = model.lambda_g / radius;
    Cc = 1. + Kn * (A1_t + A2_t * std::exp(-A3_t / Kn));
    f = 3.0 * PI * model.mu_g * (2.0 * radius) / Cc;

    double Cc_r = 1. + Kn * (A1_r + A2_r * std::exp(-A3_r / Kn));
    f_rot = 8.0 * PI * model.mu_g * std::pow(radius, 3) / Cc_r;
    
    Di = k_B * model.T_g / f;
    alpha = f / m;
    d_time = model.LD_dt_factor * m / f; // pow((LD_dt_factor*(2.0*Rp)),2)/(2*Di);
    tau = m / f;

    G = (k_B * model.T_g / m) * (1. - std::exp(-2. * alpha * d_time));
    H = (k_B * model.T_g / m) * (1. / alpha) * std::pow((1. - std::exp(-alpha * d_time)), 2.0);
    I = (k_B * model.T_g / m) * (1. / std::pow(alpha, 2.0)) * (2. * alpha * d_time - 3. + 4. * std::exp(-alpha * d_time) - std::exp(-2. * alpha * d_time));
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0;
    langevin_check = true;

    v[0] = v[1] = v[2] = (k_B * model.T_g / m);
    v_ini[0] = v_ini[1] = v_ini[2] = (k_B * model.T_g / m);
    c_rms = std::sqrt(3.0 * k_B * model.T_g / m);
    t_res = 0.0;
    r_seed = 0.0;
    r_c = 2.0 * radius;
    rg = std::sqrt(3.0 / 5.0) * radius;
    df = 3.0;
    Np = 1;

    lambda_p = std::sqrt(18. * Di * tau);
}
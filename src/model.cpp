/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "model.hpp"

using namespace std;

Model::Model() : MM(0.0),
                 Rho_p(0.0),
                 N_runs(0),
                 Box_L_factor_dmax(0.0),
                 T_g(0.0),
                 P_g(0.0),
                 phi_p(0.0),
                 mu_g(0.0),
                 lambda_g(0.0),
                 run_taus_min(0),
                 LD_dt_factor(0.0),
                 A(0.0),
                 s_LJ(0.0),
                 alpha_dp(0.0),
                 alpha_dp_max(0.0),
                 factor_vdw(0.0),
                 vdw_cutoff(0.0),
                 with_sticking(true),
                 with_rel_motion(true),
                 with_rotation(false),
                 with_brownian(true),
                 path("")
{
}

Model::Model(std::string new_path)
{
    MM = 0.0;
    Rho_p = 0.0;
    N_runs = 0;
    Box_L_factor_dmax = 0.0;
    T_g = 0.0;
    P_g = 0.0;
    phi_p = 0.0;
    mu_g = 0.0;
    lambda_g = 0.0;
    run_taus_min = 0;
    LD_dt_factor = 0.0;
    A = 0.0;
    s_LJ = 0.0;
    alpha_dp = 0.0;
    alpha_dp_max = 0.0;
    factor_vdw = 0.0;
    vdw_cutoff = 0.0;
    with_sticking = true;
    with_rel_motion = true;
    with_rotation = false;
    with_brownian = true;
    path = new_path;
}

void Model::Determine_lambda_mu()
{
    mu_g = mu_0 * ((T0 + Su) / (T_g + Su)) * std::pow(T_g / T0, 3. / 2.);    // 18.203E-6*(293.15+110)/(T+110)*pow(T/293.15,1.5);
    lambda_g = 2. * mu_g / (P_g * std::pow(8. * MM / (PI * Ru * T_g), 0.5)); // 66.5E-9*(101300/P)*(T/293.15)*(1+110/293.15)/(1+110/T);
}

void Model::Set_properties_default()
{
    //! *********** PARAMETERS PARTICLE PROPERTIES *********** !
    MM = 28.9647;  //! Molecular weight of fluid molecules (kg/kmol)
    Rho_p = 1800.; //! PPs mass density

    //! *********** SIMULATION PARAMETERS *********** !
    N_runs = 1;
    Box_L_factor_dmax = 10; //! Box size as factor of D_p
    T_g = 1700.;            //! Fluid temperature
    P_g = 101300.;          //! Fluid pressure
    phi_p = 0.0001 / 100;   //! Particles number concentration (1000 ppm = 0.01)

    Determine_lambda_mu();

    //! *********** LANGEVIN PARAMETERS *********** !
    run_taus_min = 120; // Keep in 100 to be accurate with LD theory (otherwise the 6*D*t is not strictly respected)
    LD_dt_factor = 0.1;

    //! *********** PARAMETERS INT. POTENTIALS *********** !
    A = 2.38e-19;    //! Hamaker constant (J) 2.38e-19
    s_LJ = 5.95e-10; //! LJ repulsive form parameter
    alpha_dp = 0.1;
    alpha_dp_max = 1;
    factor_vdw = 1.04;
    D_pp = 20e-09;
    vdw_cutoff = 10;
    with_sticking = true;
    with_rel_motion = true;
    with_rotation = false;
    with_brownian = true;
    //path = "/home/jose/Desktop/Gitlab/mitacs/mitacs_project/Python_simulations/aggregates_DLCA";
}

void Model::Set_properties(double new_MM,
                           double new_Rhop,
                           size_t new_N_runs,
                           double new_box_factor,
                           double new_T,
                           double new_P,
                           double new_phi,
                           size_t new_runs_taus,
                           double new_LD_fact,
                           double new_A,
                           double new_S_LJ,
                           double new_alpha_dp,
                           double new_alpha_dp_max,
                           double new_factor_vdw,
                           double new_D_pp,
                           double new_vdw_cutoff,
                           bool new_with_collisions,
                           bool new_with_rel_motion,
                           bool new_with_rotation,
                           bool new_with_brownian,
                           std::string new_path)
{
    MM = new_MM;
    Rho_p = new_Rhop;
    N_runs = new_N_runs;
    Box_L_factor_dmax = new_box_factor;
    T_g = new_T;
    P_g = new_P;
    phi_p = new_phi;
    path = new_path;
    Determine_lambda_mu();
    run_taus_min = new_runs_taus;
    LD_dt_factor = new_LD_fact;
    A = new_A;
    s_LJ = new_S_LJ;
    alpha_dp = new_alpha_dp;
    alpha_dp_max = new_alpha_dp_max;
    factor_vdw = new_factor_vdw;
    with_sticking = new_with_collisions;
    with_rel_motion = new_with_rel_motion;
    with_rotation = new_with_rotation;
    vdw_cutoff = new_vdw_cutoff;
    with_brownian = new_with_brownian;
    D_pp = new_D_pp;
}

void Model::Show_properties() const
{
    //! ********** simulation ********** !
    std::cout << "SIMULATION" << std::endl;
    std::cout << "run_taus_min:                   " << run_taus_min << std::endl;
    std::cout << "L_box/D_max                     " << Box_L_factor_dmax << std::endl;
    std::cout << "phi_p (ppm)                     " << phi_p * std::pow(10, 6) << std::endl;
    std::cout << "Path                            " << path << std::endl;
    std::cout << "alpha_dp (-, factor of Dp)      " << alpha_dp << std::endl;
    std::cout << "alpha_dp_max (-, factor of Dp)  " << alpha_dp_max << std::endl;
    std::cout << "factor_vdw (-, factor of Dp)    " << factor_vdw << std::endl;
    std::cout << "vdw_cutoff (-, factor of Rc)    " << vdw_cutoff << std::endl;
    std::cout << "with_sticking (1=yes; 0=not)    " << with_sticking << std::endl;
    std::cout << "with_rel_motion (1=yes; 0=not)  " << with_rel_motion << std::endl;
    std::cout << "with_rotation (1=yes; 0=not)    " << with_rotation << std::endl;
    std::cout << "with_brownian (1=yes; 0=not)    " << with_brownian << std::endl;
    std::cout << " " << std::endl;
    //! ********** fluid ********** !
    std::cout << "FLUID PROPERTIES" << std::endl;
    std::cout << "Temperature (K)                 " << T_g << std::endl;
    std::cout << "Pressure (Pa)                   " << P_g << std::endl;
    std::cout << "Viscosity (kg/m*s)              " << mu_g << std::endl;
    std::cout << "Mean free path (nm)             " << lambda_g * std::pow(10, 9) << std::endl;
    std::cout << "Maxwell avg. vel (m/s)          " << std::sqrt(8. * Ru * T_g / (PI * MM)) << std::endl;
    std::cout << " " << std::endl;
    //! ********** particles ********** !
    std::cout << "PARTICLES PROPERTIES            " << std::endl;
    std::cout << "Particles mean diameter (nm)    " << D_pp * (1e+09) << std::endl;
    std::cout << "Hamaker constant A/(k_b*T)      " << A / (k_B * T_g) << std::endl;
    std::cout << "Repuls. form param. s_LJ (m)    " << s_LJ  << std::endl;
    std::cout << "Bulk mass density (kg/m^3)      " << Rho_p << std::endl;
    std::cout << "Monomer diameter (nm)           " << D_pp * std::pow(10, 9) << std::endl;
    std::cout << " " << std::endl;
}
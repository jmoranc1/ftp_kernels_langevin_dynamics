/*
    FTP_Langevin: A code to simulate the collision dynamics of aerosol particles
    Copyright (C) 2022  José Morán

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "particle_list.hpp"

using namespace std;

particle_list::particle_list() : x_cm(0.0),
                                 y_cm(0.0),
                                 z_cm(0.0),
                                 mean_Rp(0.0),
                                 Rg(0.0),
                                 Rs(0.0),
                                 Rmax(0.0),
                                 Np(0.0),
                                 Rpp_v(0.0), ///< Vol-eq mean PP radius
                                 x_(0.0),
                                 y_(0.0),
                                 z_(0.0),  ///< position after rotation
                                 tau(0.0), ///< momentum relaxation time
                                 Kn(0.0),
                                 Cc(0.0), ///< Knudsen, Cunningham
                                 lambda_p(0.0),
                                 Kn_d(0.0),
                                 m(0.0),
                                 f(0.0),
                                 Di(0.0),
                                 Di_rot(0.0),
                                 alpha(0.0),
                                 d_time(0.0), ///< mass, friction, diffusion coefficient, alpha
                                 exp1(0.0),
                                 exp2(0.0),
                                 r1(0.0), ///< values for Langevin Dynamics
                                 B1_sq(0.0),
                                 B2_sq(0.0), ///< values for Langevin Dynamics
                                 f_atr(0.0),
                                 f_rep(0.0),
                                 f_mag(0.0), ///< Attractive, repulsive, and total interaction force
                                 f_point(0.0),
                                 langevin_check(true), ///< a bool to check Langevin accuracy
                                 l0(0.0),
                                 l1(0.0),
                                 l2(0.0),
                                 d_mag(0.0),             ///< unitary components, and d_mag maginitude displac.
                                 displac_collision(0.0), ///< displacement considering collision
                                 relative_distance(0.0), ///< relative displacement between agglomerates
                                 inertia_moment_mag(0.0),
                                 f_rot(0.0),
                                 tau_rot(0.0),
                                 first(true)
{
}

particle_list::particle_list(double radius)
{
    // std::vector<particle> list;
    x_cm = y_cm = z_cm = 0.0;
    mean_Rp = radius;
    Rg = 0.0;
    Rs = 0.0;
    Rmax = 0.0;
    Np = 0;
    Rpp_v = 0.0;        ///< Vol-eq mean PP radius
    x_ = y_ = z_ = 0.0; ///< position after rotation
    tau = 0.0;          ///< momentum relaxation time
    v[0] = v[1] = v[2] = 0.0;
    v_ini[0] = v_ini[1] = v_ini[2] = 0.0; ///< velocity vectorm v_ini to reestart condition when deleting particles
    Kn = 0.0;
    Cc = 0.0; ///< Knudsen, Cunningham
    lambda_p = 0.0;
    Kn_d = 0.0;
    m = f = Di = alpha = d_time = 0.0; ///< mass, friction, diffusion coefficient, alpha
    exp1 = exp2 = r1 = 0.0;            ///< values for Langevin Dynamics
    B1_sq = B2_sq = 0.0;               ///< values for Langevin Dynamics
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0; ///< Langevin position/velocity fluctuation
    f_atr = f_rep = f_mag = 0.0;          ///< Attractive, repulsive, and total interaction force
    f_point = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2]= 0.0; ///< external force vector
    langevin_check = true;          ///< a bool to check Langevin accuracy
    l0 = l1 = l2 = d_mag = 0.0;     ///< unitary components, and d_mag maginitude displac.
    displac_collision = 0.0;        ///< displacement considering collision
    relative_distance = 0.0;        ///< relative displacement between agglomerates
    inertia_moment_mag = 0.0;
    inertia_moment[0]=inertia_moment[1]=inertia_moment[2]=0.0;
    f_rot = 0.0;
    v_rot[0] = v_rot[1] = v_rot[2] = 0.0;
    n_or[0] = n_or[1] = n_or[2] = 0.0;
    n_or_old[0] = n_or_old[1] = n_or_old[2] = 0.0;
    M_ext[0] = M_ext[1] = M_ext[2] = 0.0;
    tau_rot = 0.0;
    first=true;
}

void particle_list::Reset_particle_list()
{
    // std::vector<particle> list;
    x_cm = y_cm = z_cm = 0.0;
    mean_Rp = 0;
    Rg = 0.0;
    Rs = 0.0;
    Rmax = 0.0;
    Np = 0;
    Rpp_v = 0.0;        ///< Vol-eq mean PP radius
    x_ = y_ = z_ = 0.0; ///< position after rotation
    tau = 0.0;          ///< momentum relaxation time
    v[0] = v[1] = v[2] = 0.0;
    v_ini[0] = v_ini[1] = v_ini[2] = 0.0; ///< velocity vectorm v_ini to reestart condition when deleting particles
    Kn = 0.0;
    Cc = 0.0; ///< Knudsen, Cunningham
    lambda_p = 0.0;
    Kn_d = 0.0;
    m = f = Di = alpha = d_time = 0.0; ///< mass, friction, diffusion coefficient, alpha
    exp1 = exp2 = r1 = 0.0;            ///< values for Langevin Dynamics
    B1_sq = B2_sq = 0.0;               ///< values for Langevin Dynamics
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0; ///< Langevin position/velocity fluctuation
    f_atr = f_rep = f_mag = 0.0;          ///< Attractive, repulsive, and total interaction force
    f_point = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2] = 0.0; ///< external force vector
    langevin_check = true;          ///< a bool to check Langevin accuracy
    l0 = l1 = l2 = d_mag = 0.0;     ///< unitary components, and d_mag maginitude displac.
    displac_collision = 0.0;        ///< displacement considering collision
    relative_distance = 0.0;        ///< relative displacement between agglomerates
    inertia_moment_mag = 0.0;
    inertia_moment[0]=inertia_moment[1]=inertia_moment[2]=0.0;
    f_rot = 0.0;
    v_rot[0] = v_rot[1] = v_rot[2] = 0.0;
    n_or[0] = n_or[1] = n_or[2] = 0.0;
    n_or_old[0] = n_or_old[1] = n_or_old[2] = 1 / std::sqrt(3);
    M_ext[0] = M_ext[1] = M_ext[2] = 0.0;
    tau_rot = 0.0;
    first=true;
}


void particle_list::Show_properties()
{
    std::cout << "Particle list properties      " << std::endl;
    std::cout << "Np (list.size()):             " << Np << "(" << list.size() << ")" << std::endl;
    std::cout << "Rmax (nm)                     " << Rmax * std::pow(10, 9) << std::endl;
    std::cout << "Rs (nm)                       " << Rs * std::pow(10, 9) << std::endl;
    std::cout << "Rg (nm; DLCA approx.)         " << Rg * std::pow(10, 9) << "("
                                                  << mean_Rp * std::pow(Np/1.4,1/1.78) * std::pow(10, 9) << ")" << std::endl;
    std::cout << "Rg/Rp                         " << Rg / mean_Rp << std::endl;
    std::cout << "Rpp_v (nm)                    " << Rpp_v * std::pow(10, 9) << std::endl;
    std::cout << "mean_Rp (nm)                  " << mean_Rp * std::pow(10, 9) << std::endl;
    std::cout << "lambda_p (nm)                 " << lambda_p * std::pow(10, 9) << std::endl;
    std::cout << "m (fg)                        " << m * std::pow(10, 15) << std::endl;
    std::cout << "Kn_gas (-)                    " << Kn << std::endl;
    std::cout << "Kn_diff (-)                   " << Kn_d << std::endl;
    std::cout << "tau (us)                      " << tau * std::pow(10, 6) << std::endl;
    std::cout << "tau_rot (us)                  " << tau_rot * std::pow(10, 6) << std::endl;
    std::cout << "tau_rot/tau                   " << tau_rot / tau << std::endl;
    std::cout << "d_time (us)                   " << d_time * std::pow(10, 6) << std::endl;
    std::cout << "langevin_check                " << langevin_check << std::endl;
    std::cout << "rotational f_r (kg*m^2/s)     " << f_rot << " (" << f_rot / list[0].f_rot << ")" << std::endl;
    std::cout << "translational f_t (kg/s)      " << f << " (" << f / list[0].f << ")" << std::endl;
    std::cout << "f_rot/f_trans                 " << f_rot / f << std::endl;
    std::cout << "(x_cm, y_cm, z_cm)            " << x_cm << " " << y_cm << " " << z_cm << std::endl;
    std::cout << "orientation n_or              " << n_or[0] << " " << n_or[1] << " " << n_or[2] << std::endl;
    std::cout << "v_translation                 " << v[0] << " " << v[1] << " " << v[2] << std::endl;
    std::cout << "v_rotation                    " << v_rot[0] << " " << v_rot[1] << " " << v_rot[2] << std::endl;
    std::cout << "relative_distance             " << relative_distance << std::endl;
    std::cout << "F_vdW (N)                     " << f_mag << std::endl;
    std::cout << "F_vdW/F_grav                  " << f_mag / (m * 9.81) << std::endl;
    std::cout << "F_vdW/F_point                 " << f_mag / f_point << std::endl;
    std::cout << "M_ext (N*m)                   " << M_ext[0] << " " << M_ext[1] << " " << M_ext[2] << std::endl;
    std::cout << "inertia_moment (kg*m^2)       " << inertia_moment_mag << std::endl;
    std::cout << " "                              << std::endl;
}

void particle_list::Show_coordinates()
{
    std::cout << "Properties of particles: (x, y, z), Rp      " << std::endl;
    for (const auto &p : list)
    {
        std::cout << "(" << p.x << ", " << p.y << ", " << p.z << "), " << p.Rp << std::endl;
    }
}

void particle_list::Copy(particle_list in_spheres, const Model model)
{
    list.clear();
    for (const auto &p : in_spheres.list)
    {
        list.push_back(particle(p.x, p.y, p.z, p.Rp, model));
    }
    x_cm = in_spheres.x_cm;
    y_cm = in_spheres.y_cm;
    z_cm = in_spheres.z_cm;
    mean_Rp = in_spheres.mean_Rp;
    Rg = in_spheres.Rg;
    Rs = in_spheres.Rs;
    Rmax = in_spheres.Rmax;
    Np = in_spheres.Np;
    Rpp_v = in_spheres.Rpp_v; ///< Vol-eq mean PP radius
    x_ = in_spheres.x_;
    y_ = in_spheres.y_;
    z_ = in_spheres.z_;   ///< position after rotation
    tau = in_spheres.tau; ///< momentum relaxation time
    v[0] = in_spheres.v[0];
    v[1] = in_spheres.v[1];
    v[2] = in_spheres.v[2];
    v_ini[0] = in_spheres.v_ini[0];
    v_ini[1] = in_spheres.v_ini[1];
    v_ini[2] = in_spheres.v_ini[2]; ///< velocity vectorm v_ini to reestart condition when deleting particles
    Kn = in_spheres.Kn;
    Cc = in_spheres.Cc; ///< Knudsen, Cunningham
    Kn_d = in_spheres.Kn_d;
    m = in_spheres.m;
    f = in_spheres.f;
    Di = in_spheres.Di;
    Di_rot = in_spheres.Di_rot;
    alpha = in_spheres.alpha;
    d_time = in_spheres.d_time; ///< mass, friction, diffusion coefficient, alpha
    exp2 = in_spheres.exp2;
    exp1 = in_spheres.exp1;
    r1 = in_spheres.r1;       ///< values for Langevin Dynamics
    B1_sq = in_spheres.B1_sq; ///< values for Langevin Dynamics
    B2_sq = in_spheres.B2_sq; ///< values for Langevin Dynamics
    R_ran[0] = in_spheres.R_ran[0];
    R_ran[1] = in_spheres.R_ran[1];
    R_ran[2] = in_spheres.R_ran[2];
    V_ran[0] = in_spheres.V_ran[0];
    V_ran[1] = in_spheres.V_ran[1];
    V_ran[2] = in_spheres.V_ran[2]; ///< Langevin position/velocity fluctuation
    f_atr = in_spheres.f_atr;
    f_rep = in_spheres.f_rep;
    f_mag = in_spheres.f_mag; ///< Attractive, repulsive, and total interaction force
    f_point = in_spheres.f_point;
    F_ext[0] = in_spheres.F_ext[0];
    F_ext[1] = in_spheres.F_ext[1];
    F_ext[2] = in_spheres.F_ext[2];             ///< external force vector
    langevin_check = in_spheres.langevin_check; ///< a bool to check Langevin accuracy
    l0 = in_spheres.l0;
    l1 = in_spheres.l1;
    l2 = in_spheres.l2;
    d_mag = in_spheres.d_mag;                         ///< unitary components, and d_mag maginitude displac.
    displac_collision = in_spheres.displac_collision; ///< displacement considering collision
    relative_distance = in_spheres.relative_distance; ///< relative displacement between agglomerates
    inertia_moment_mag = in_spheres.inertia_moment_mag;
    inertia_moment[0] = in_spheres.inertia_moment[0];
    inertia_moment[1] = in_spheres.inertia_moment[1];
    inertia_moment[2] = in_spheres.inertia_moment[2];
    f_rot = in_spheres.f_rot;
    v_rot[0] = in_spheres.v_rot[0];
    v_rot[1] = in_spheres.v_rot[1];
    v_rot[2] = in_spheres.v_rot[2];
    n_or[0] = in_spheres.n_or[0];
    n_or[1] = in_spheres.n_or[1];
    n_or[2] = in_spheres.n_or[2];
    n_or_old[0] = in_spheres.n_or_old[0];
    n_or_old[1] = in_spheres.n_or_old[1];
    n_or_old[2] = in_spheres.n_or_old[2];
    M_ext[0] = in_spheres.M_ext[0];
    M_ext[1] = in_spheres.M_ext[1];
    M_ext[2] = in_spheres.M_ext[2];
    tau_rot = in_spheres.tau_rot;
    first=in_spheres.first;
}

void particle_list::Set_particle_list(const std::vector<particle> part,
                                      const Model model)
{
    v[0] = v[1] = v[2] = 0.0;
    v_ini[0] = v_ini[1] = v_ini[2] = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2] = 0.0;
    M_ext[0] = M_ext[1] = M_ext[2] = 0.0;
    v_rot[0] = v_rot[1] = v_rot[2] = 0.0;
    n_or[0] = n_or[1] = n_or[2] = 0.0;
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0;
    n_or_old[0] = n_or_old[1] = n_or_old[2] = 1 / std::sqrt(3);
    list.clear();
    for (const auto &p : part)
    {
        list.push_back(particle(p.x, p.y, p.z, p.Rp, model));
    }
    Update_particle_list(model);
    rotate_random();
}

void particle_list::Update_particle_list(const Model model)
{
    Np = list.size();
    Set_new_PP_radius(model.D_pp * 0.5);
    Get_mean_rp();
    Get_center_mass();
    Get_radius_gyration();
    Get_inertia_moment_tensor();
    Get_maximum_radius();
    Physical_properties(model);
}

/********************************************************************************
 * Set new PP diameter
 ********************************************************************************/
void particle_list::Set_new_PP_radius(const double new_Rp)
{
    // Reset particle list
    mean_Rp = 0.0;
    for (const auto &p : list)
    {
        mean_Rp += p.Rp;
    }
    mean_Rp /= static_cast<double>(list.size());

    for (auto &p : list)
    {
        p.x = p.x * new_Rp/mean_Rp;
        p.y = p.y * new_Rp/mean_Rp;
        p.z = p.z * new_Rp/mean_Rp;
        p.Rp = p.Rp * new_Rp/mean_Rp;
    }
}

/********************************************************************************
 * Properties of the list of particles
 ********************************************************************************/
void particle_list::Get_inertia_moment_tensor()
{
    if (Np == 1)
    {
        inertia_moment_mag = (2 / 5) * m * std::pow(mean_Rp, 2);
        inertia_moment[0]=inertia_moment[1]=inertia_moment[2]=inertia_moment_mag/3;
        Rg = std::sqrt(3. / 5.) * mean_Rp;
        return;
    }

    inertia_moment[0]=0.0;
    inertia_moment[1]=0.0;
    inertia_moment[2]=0.0;

    for (const auto &p : list)
    {
        double x1 = p.x - x_cm;
        double y1 = p.y - y_cm;
        double z1 = p.z - z_cm;

        inertia_moment[0] += p.m * (std::pow(y1, 2) + std::pow(z1, 2));
        inertia_moment[1] += p.m * (std::pow(x1, 2) + std::pow(z1, 2));
        inertia_moment[2] += p.m * (std::pow(x1, 2) + std::pow(y1, 2));
    }

    inertia_moment_mag = std::sqrt(std::pow(inertia_moment[0],2)+
                                   std::pow(inertia_moment[1],2)+
                                   std::pow(inertia_moment[2],2));
}
void particle_list::Get_mean_rp()
{
    if (Np == 1)
    {
        mean_Rp = list[0].Rp;
        Rpp_v = list[0].Rp;
        return;
    }
    double sum_rp(0.0), sum_rp3(0.0);
    int counter = 0;

    for (const auto &p : list)
    {
        sum_rp += p.Rp;
        sum_rp3 += std::pow(p.Rp, 3);
        counter += 1;
    };

    mean_Rp = sum_rp / static_cast<double>(counter);
    Rpp_v = std::pow(sum_rp3 / static_cast<double>(counter), 1. / 3.);
}
void particle_list::Get_radius_gyration()
{
    if (Np == 1)
    {
        Rg = std::sqrt(3.0 / 5.0) * list[0].Rp;
        return;
    }
    // FIRST CALCULATE CENTER OF MASS
    double xcm = 0;
    double ycm = 0;
    double zcm = 0;
    int counter(0);

    for (const auto &p : list)
    {
        xcm = xcm + p.x;
        ycm = ycm + p.y;
        zcm = zcm + p.z;

        counter += 1;
    }
    xcm = xcm / double(counter);
    ycm = ycm / double(counter);
    zcm = zcm / double(counter);

    // SECOND, CALCULATE THE RADIUS OF GYRATION
    double rxcm, rycm, rzcm;
    double rcm = 0.0;
    double sum_mass = 0.0;

    for (const auto &p : list)
    {
        rxcm = std::pow(p.x - xcm, 2.0);
        rycm = std::pow(p.y - ycm, 2.0);
        rzcm = std::pow(p.z - zcm, 2.0);

        //inertia_moment += p.m * (rxcm + rycm + rzcm + 2.0 / 5.0 * p.Rp * p.Rp);
        rcm += p.m * (rxcm + rycm + rzcm + 3.0 / 5.0 * p.Rp * p.Rp);
        sum_mass += p.m;
    };

    Rg = std::sqrt(rcm / sum_mass);
}
void particle_list::Get_center_mass()
{
    if (Np == 1)
    {
        x_cm = list[0].x;
        y_cm = list[0].y;
        z_cm = list[0].z;
        return;
    }
    double xcm = 0;
    double ycm = 0;
    double zcm = 0;
    int counter(0);

    for (const auto &p : list)
    {
        xcm += p.x;
        ycm += p.y;
        zcm += p.z;

        counter += 1;
    }
    x_cm = xcm / static_cast<double>(counter);
    y_cm = ycm / static_cast<double>(counter);
    z_cm = zcm / static_cast<double>(counter);
}
void particle_list::Get_maximum_radius()
{
    if (Np == 1)
    {
        Rmax = list[0].Rp;
        return;
    }
    double temp = 0;
    double res = 0;

    double dx, dy, dz;
    for (const auto &p : list)
    {
        dx = p.x - x_cm;
        dy = p.y - y_cm;
        dz = p.z - z_cm;
        temp = std::sqrt(dx * dx + dy * dy + dz * dz) + p.Rp;
        if (temp > res)
        {
            res = temp;
        };
    };
    Rmax = res;
}

void particle_list::Physical_properties(const Model model)
{
    x_ = x_cm;
    y_ = y_cm;
    z_ = z_cm;
    l0 = l1 = l2 = 0.0;
    d_mag = 0.0;
    displac_collision = 0.0;
    relative_distance = 0.0;
    f_atr = f_rep = f_mag = 0.0;
    F_ext[0] = F_ext[1] = F_ext[2] = 0.0;

    m = Np * std::pow(Rpp_v, 3) * (4. * PI / 3.) * model.Rho_p;
    friction_coeff_CORSON(model);
    Smoluchowski_radius(model);

    Kn = model.lambda_g / Rs;                            // based on rg
    Cc = 1. + Kn * (A1_t + A2_t * std::exp(-A3_t / Kn)); // based on rg

    Di = k_B * model.T_g / f;
    Di_rot = k_B * model.T_g / f_rot;
    alpha = f / m;
    d_time = model.LD_dt_factor * m / f; // pow((LD_dt_factor*(2.0*Rp)),2)/(2*Di);
    tau = m / f;
    tau_rot = inertia_moment_mag / f_rot;

    exp2 = std::exp(-2.0 * alpha * d_time);
    exp1 = std::exp(-alpha * d_time);
    r1 = (1.0 - exp1) / (1.0 + exp1);
    B1_sq = std::sqrt((k_B * model.T_g / m) * (1.0 - exp2));
    B2_sq = std::sqrt((2.0 * k_B * model.T_g / m) *
                      (1.0 / std::pow(alpha, 2)) *
                      (alpha * d_time - 2.0 * r1));
    R_ran[0] = R_ran[1] = R_ran[2] = 0.0;
    V_ran[0] = V_ran[1] = V_ran[2] = 0.0;
    langevin_check = true;

    lambda_p = std::sqrt(18. * Di * tau);
    Kn_d = lambda_p / Rs;

    v_ini[0] = v[0];
    v_ini[1] = v[1];
    v_ini[2] = v[2];
}

/********************************************************************************
 * obtain the friction cofficient of aggregates based on Corson et al. model
 ********************************************************************************/
void particle_list::friction_coeff_CORSON(const Model model)
{
    double Kn = model.lambda_g / mean_Rp;
    double cc_t = 1. + Kn * (A1_t + A2_t * std::exp(-A3_t / Kn));
    double temp_t = (1. + 1.612 * Kn) * std::pow(std::pow(0.852 * std::pow(double(Np), 0.535) + 0.148, -1) +\
                    1.612 * Kn * std::pow(0.843 * std::pow(double(Np), 0.939) + 0.157, -1), -1);
    f = (6.0 * PI * model.mu_g * mean_Rp / cc_t) * temp_t;

    double cc_r = 1. + Kn * (A1_r + A2_r * std::exp(-A3_r / Kn));
    double temp_r = (1. + 5.988 * Kn) * std::pow(std::pow(0.713 * std::pow(double(Np), 1.63) + 0.287, -1) +\
                    5.988 * Kn * std::pow(1.184 * std::pow(double(Np), 2.02) - 0.184, -1), -1);
    //f_rot = (8.0 * PI * model.mu_g * std::pow(mean_Rp, 3) / cc_r) * temp_r;
    f_rot = (8.0 * PI * model.mu_g * std::pow(mean_Rp, 3) / cc_r) * double(Np);
}

double particle_list::Friction_sphere(const double R, const Model model)
{
    double Kn = model.lambda_g / R;
    double cc_t = 1. + Kn * (A1_t + A2_t * std::exp(-A3_t / Kn));
    return 6.0 * PI * model.mu_g * R / cc_t;
}
void particle_list::Smoluchowski_radius(const Model model)
{
    Rs = Bisection(Rmax, model);
}
double particle_list::Bisection(const double R0, const Model model)
{
    double 	rmin, rmax, rmed, frmed, frmin, frmax;
    rmin = R0/10;
    rmax = 10*R0;

    double precision = 1e-05 * Friction_sphere(R0, model);

    frmin = Friction_sphere(rmin, model) - f;
    frmax = Friction_sphere(rmax, model) - f;

    rmed = (rmin+rmax)/2 ;
    frmed = Friction_sphere(rmed, model) - f;

    while (fabs(frmed)>precision)
    {
        if (frmin*frmax>=0)
        {
            std::cout << "Incorrect interval : " << frmin << " " << frmax << std::endl;
            return -1;
        }
        if (frmin*frmed < 0)
        {
            rmax = rmed;
            frmax = frmed;
        }
        else
        {
            rmin = rmed;
            frmin = frmed;
        }
        rmed = (rmin+rmax)/2 ;
        frmed = Friction_sphere(rmed, model) -f;
    }
    return rmed;
}

void particle_list::new_VR_ran(const Model model,
                               const double time_step)
{
    exp2 = std::exp(-2.0 * alpha * time_step);
    exp1 = std::exp(-alpha * time_step);
    r1 = (1.0 - exp1) / (1.0 + exp1);
    B1_sq = std::sqrt((3.0 * k_B * model.T_g / m) * (1.0 - exp2) / 3.);
    B2_sq = std::sqrt((6.0 * k_B * model.T_g / m) *
                      (1.0 / std::pow(alpha, 2)) *
                      (alpha * time_step - 2.0 * r1) / 3.);

    for (size_t k = 0; k < 3; k++)
    {
        V_ran[k] = B1_sq * Gaussian(0., 1.);
        R_ran[k] = B2_sq * Gaussian(0., 1.);
    }
}

void particle_list::Get_particles(double x[],
                                  double y[],
                                  double z[],
                                  double r[])
{
    size_t i = 0;
    for (const auto &p : list)
    {
        x[i] = p.x;
        y[i] = p.y;
        z[i] = p.z;
        r[i] = p.Rp;
        i++;
    }
}

std::vector<double> particle_list::Get_particles_x()
{
    std::vector<double> prop_ev;
    size_t i = 0;
    for (const auto &p : list)
    {
        prop_ev.push_back(p.x);
        i++;
    }
    return prop_ev;
}
std::vector<double> particle_list::Get_particles_y()
{
    std::vector<double> prop_ev;
    size_t i = 0;
    for (const auto &p : list)
    {
        prop_ev.push_back(p.y);
        i++;
    }
    return prop_ev;
}
std::vector<double> particle_list::Get_particles_z()
{
    std::vector<double> prop_ev;
    size_t i = 0;
    for (const auto &p : list)
    {
        prop_ev.push_back(p.z);
        i++;
    }
    return prop_ev;
}
std::vector<double> particle_list::Get_particles_Rp()
{
    std::vector<double> prop_ev;
    size_t i = 0;
    for (const auto &p : list)
    {
        prop_ev.push_back(p.Rp);
        i++;
    }
    return prop_ev;
}

double particle_list::Gaussian(double mu, double sigma)
{
    static double sqrt2logx1, twopix2;

    if (first)
    {
        first = false;
        sqrt2logx1 = std::sqrt(-2 * std::log(drand48()));
        twopix2 = 2 * PI * drand48();
        return mu + sigma * sqrt2logx1 * std::cos(twopix2);
    }
    else
    {
        first = true;
        return mu + sigma * sqrt2logx1 * std::sin(twopix2);
    }
}
/********************************************************************************
 * Rotate an agglomerate
 ********************************************************************************/
void particle_list::rotate_agglomerate_axes()
{
    if (Np == 1)
    {
        return;
    }
    std::array<double, 3> thetas = Angles_rotation();
    double theta_x = thetas[0];
    double theta_y = thetas[1];
    double theta_z = thetas[2];
    // Components of the rotation matrix
    double R_x[3][3] = {{1., 0., 0.},
                        {0., std::cos(theta_x), -std::sin(theta_x)},
                        {0., std::sin(theta_x), std::cos(theta_x)}};
    double R_y[3][3] = {{std::cos(theta_y), 0., std::sin(theta_y)},
                        {0., 1., 0.},
                        {-std::sin(theta_y), 0., std::cos(theta_y)}};
    double R_z[3][3] = {{std::cos(theta_z), -std::sin(theta_z), 0.},
                        {std::sin(theta_z), std::cos(theta_z), 0.},
                        {0., 0., 1.}};

    // Perform rotation
    for (size_t i = 0; i < Np; i++)
    {
        double asd[3] = {list[i].x - x_cm,
                         list[i].y - y_cm,
                         list[i].z - z_cm};
        double temp[3][3] = {{0., 0., 0.},
                             {0., 0., 0.},
                             {0., 0., 0.}};
        matmul_33(R_z, R_y, temp);
        double temp2[3][3] = {{0., 0., 0.},
                              {0., 0., 0.},
                              {0., 0., 0.}};
        matmul_33(temp, R_x, temp2);
        double Mult[3] = {0., 0., 0.};
        matmul_31(temp2, asd, Mult);
        list[i].x = x_cm + Mult[0];
        list[i].y = y_cm + Mult[1];
        list[i].z = z_cm + Mult[2];
        list[i].x_ = list[i].x;
        list[i].y_ = list[i].y;
        list[i].z_ = list[i].z;
    }
    n_or_old[0] = n_or[0];
    n_or_old[1] = n_or[1];
    n_or_old[2] = n_or[2];
    return;
}
void particle_list::rotate_random()
{
    std::array<double, 3> p_sphere;
    Rand_point_sphere(p_sphere);
    n_or[0] = p_sphere[0];
    n_or[1] = p_sphere[1];
    n_or[2] = p_sphere[2];
    rotate_agglomerate_axes();
    return;
}

double particle_list::rand_01() { return double(rand()) / (RAND_MAX); };

/********************************************************************************
 * Random point in a sphere (Not biased, from Wolfram)
 ********************************************************************************/
void particle_list::Rand_point_sphere(std::array<double, 3> &point)
{
    // -- Generating a random direction --
    double thetarandom = rand_01() * PI * 2;
    double phirandom = std::acos(1 - 2 * rand_01());
    point = {{std::sin(phirandom) * std::cos(thetarandom),
              std::sin(phirandom) * std::sin(thetarandom),
              std::cos(phirandom)}};
}

/********************************************************************************
 * Rotation matrix angles from a point in sphere
 ********************************************************************************/
double particle_list::Get_theta(double n1[2], double n2[2])
{
    double x = n1[0] * n2[0] + n1[1] * n2[1]; // dot product
    double y = n1[0] * n2[1] + n1[1] * n2[0]; // magnitude of cross product
    return std::atan2(y,x);
}
std::array<double, 3> particle_list::Angles_rotation()
{
    double n_or_unit[3], n_or_old_unit[3];
    double n_or_mag = std::sqrt(std::pow(n_or[0],2)+std::pow(n_or[1],2)+std::pow(n_or[2],2));
    n_or_unit[0] = n_or[0]/n_or_mag;
    n_or_unit[1] = n_or[1]/n_or_mag;
    n_or_unit[2] = n_or[2]/n_or_mag;
    double n_or_old_mag = std::sqrt(std::pow(n_or[0],2)+std::pow(n_or[1],2)+std::pow(n_or[2],2));
    n_or_old_unit[0] = n_or_old[0]/n_or_old_mag;
    n_or_old_unit[1] = n_or_old[1]/n_or_old_mag;
    n_or_old_unit[2] = n_or_old[2]/n_or_old_mag;

    double theta_x(0.), theta_y(0.), theta_z(0.);
    // x - axis
    double n1x[2] = {n_or_unit[1], n_or_unit[2]};
    double n2x[2] = {n_or_old_unit[1], n_or_old_unit[2]};
    theta_x = Get_theta(n1x, n2x);
    // y - axis
    double n1y[2] = {n_or_unit[0], n_or_unit[2]};
    double n2y[2] = {n_or_old_unit[0], n_or_old_unit[2]};
    theta_y = Get_theta(n1y, n2y);
    // z - axis
    double n1z[2] = {n_or_unit[0], n_or_unit[1]};
    double n2z[2] = {n_or_old_unit[0], n_or_old_unit[1]};
    theta_z = Get_theta(n1z, n2z);
    return {theta_x, theta_y, theta_z};
}
void particle_list::matmul_31(const double A[3][3],
                              const double B[3],
                              double (&mult)[3])
{
    for (size_t i = 0; i < 3; i++)
    {
        mult[i] = 0.;
    }

    for (size_t i = 0; i < 3; i++)
        for (size_t k = 0; k < 3; k++)
        {
            mult[i] += A[i][k] * B[k];
        }
}
void particle_list::matmul_33(const double A[3][3],
                              const double B[3][3],
                              double (&mult)[3][3])
{
    for (size_t i = 0; i < 3; i++)
        for (size_t j = 0; j < 3; j++)
        {
            mult[i][j] = 0.;
        }

    for (size_t i = 0; i < 3; i++)
        for (size_t j = 0; j < 3; j++)
            for (size_t k = 0; k < 3; k++)
            {
                mult[i][j] += A[i][k] * B[k][j];
            }
    return;
}
